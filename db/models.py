import datetime

from peewee import *

from config import *

from loader import dp

db = SqliteDatabase(DB_PATH)

class Eforia(Model):
    telega_id = IntegerField()
    username = TextField()

    old = IntegerField()
    people = IntegerField()
    food = IntegerField()
    land = IntegerField()
    money = IntegerField()
    army = IntegerField()

    p_war = IntegerField()
    p_hunger = IntegerField()
    p_caravan = IntegerField()

    decret2food = IntegerField() #на еду
    decret2seed = IntegerField() #на посев
    decret2caravan = IntegerField() #на караван
    decret2army = IntegerField()  # нанять наемников

    buy_food = IntegerField() #купить еду
    buy_land = IntegerField()  # купить землю
    sell_food = IntegerField() #купить еду
    sell_land = IntegerField()  # купить землю

    food_rate = IntegerField()  # ставка зерно
    land_rate = IntegerField()  # ставка земля
    food_vol = IntegerField()  # объем зерно
    land_vol = IntegerField()  # объем земля

    wait_migrate = IntegerField() #мигранты на след.год
    wait_w_harvest = FloatField()  # мигранты на след.год

    garmony = FloatField()

    log_garmony = TextField()
    log_people = TextField()
    log_land = TextField()
    log_army = TextField()
    log_money = TextField()
    log_event = TextField()

    cookie = TextField()


    # insert into Cert values (null, 1986546, 'И', 'И', 'И', 40, 675, '22.09.2001', 'c9d55354-ecbe-44a6-87d7-422309e6aeab')

    class Meta:
        database = db  # модель будет использовать базу данных в DB_PATH




