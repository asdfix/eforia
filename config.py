from environs import Env

# Теперь используем вместо библиотеки python-dotenv библиотеку environs
env = Env()
env.read_env()

BOT_TOKEN = env.str("BOT_TOKEN")    # Забираем значение типа str
ADMINS = env.list("ADMINS")         # Тут у нас будет список из админов
IP = env.str("ip")                  # Тоже str, но для айпи адреса хоста

DB_PATH = env.str("sqlite_path")

LOG_PATH = env.str("LOG_PATH")
LOG_LEVEL = env.str("LOG_LEVEL")

IMG_PATH = env.str("IMG_PATH")


SERVER_IP = env.str("SERVER_IP")
SERVER_PORT = env.str("SERVER_PORT")


