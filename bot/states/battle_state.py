from aiogram.dispatcher.filters.state import State, StatesGroup

class battle_state(StatesGroup):
    in_progress = State()
    select_unit = State()
    move_unit = State()
    finished = State()
