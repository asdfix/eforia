from aiogram.dispatcher.filters.state import State, StatesGroup


class puzzle_state(StatesGroup):
    start = State()
    waiting_f = State()
    waiting_i = State()
    waiting_o = State()
    waiting_pass_ser = State()
    waiting_pass_no = State()
    waiting_birthday = State()
    waiting_region = State()
    check_data = State()
