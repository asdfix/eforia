from aiogram.dispatcher.filters.state import State, StatesGroup

class decret_state(StatesGroup):
    start = State()
    waiting_food = State()
    waiting_seed = State()
    waiting_caravan = State()
    waiting_army = State()
    waiting_del = State()
    check_data = State()

