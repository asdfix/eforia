from aiogram.dispatcher.filters.state import State, StatesGroup

class exchange_state(StatesGroup):
    start = State()
    waiting_buy_food = State()
    waiting_buy_land = State()
    waiting_sell_food = State()
    waiting_sell_land = State()
    check_data = State()
