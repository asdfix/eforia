import emoji
from aiogram import types

from bot.handlers.cbdata import callback_puzzle


def puzzle_menu(message):
    # Генерация menu.
    buttons = []

    buttons.append(types.InlineKeyboardButton(text="Пропустить пока",
                                              callback_data=callback_puzzle.new(modle="puzzle", action="skip")))
    buttons.append(
        types.InlineKeyboardButton(text="Подсказку!", callback_data=callback_puzzle.new(modle="puzzle", action="hint")))

    # Благодаря row_width=2, в первом ряду будет две кнопки, а оставшаяся одна
    # уйдёт на следующую строку
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    keyboard.add(*buttons)
    return keyboard


def puzzle_hint_menu(message):
    # Генерация menu.
    buttons = []

    buttons.append(types.InlineKeyboardButton(text="Пропустить пока",
                                              callback_data=callback_puzzle.new(modle="puzzle", action="skip")))
    buttons.append(
        types.InlineKeyboardButton(text="В начало", callback_data=callback_puzzle.new(modle="puzzle", action="start")))

    # Благодаря row_width=2, в первом ряду будет две кнопки, а оставшаяся одна
    # уйдёт на следующую строку
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    keyboard.add(*buttons)
    return keyboard


def next_menu(message):
    # Генерация menu.
    buttons = []

    buttons.append(
        types.InlineKeyboardButton(text="Еще!", callback_data=callback_puzzle.new(modle="puzzle", action="go")))
    buttons.append(types.InlineKeyboardButton(text="Статистика",
                                              callback_data=callback_puzzle.new(modle="puzzle", action="stats")))

    # Благодаря row_width=2, в первом ряду будет две кнопки, а оставшаяся одна
    # уйдёт на следующую строку
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    keyboard.add(*buttons)
    return keyboard


def stats_menu(message):
    # Генерация menu.
    buttons = []

    buttons.append(types.InlineKeyboardButton(text=emoji.emojize(":OK_hand:Играть!:red_exclamation_mark:"),
                                              callback_data=callback_puzzle.new(modle="puzzle", action="go")))
    buttons.append(types.InlineKeyboardButton(text="Сброс",
                                              callback_data=callback_puzzle.new(modle="puzzle", action="clear_stats")))

    # Благодаря row_width=2, в первом ряду будет две кнопки, а оставшаяся одна
    # уйдёт на следующую строку
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    keyboard.add(*buttons)
    return keyboard
