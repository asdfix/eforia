import emoji
from aiogram import types

from bot.handlers.cbdata import callback_puzzle, callback_admin, callback_rules  #
from bot.utils.misc.logging import logger


def uni_menu(buttons_data, rows=2):
    buttons = []

    for button_data in buttons_data:
        txt, cbdata = button_data
        buttons.append(types.InlineKeyboardButton(text=txt, callback_data=cbdata))

    keyboard = types.InlineKeyboardMarkup(row_width=rows)
    keyboard.add(*buttons)
    return keyboard


def main_menu(message):
    logger.debug(message.chat.id)

    # Генерация menu.
    buttons = [
        # types.InlineKeyboardButton(text=emoji.emojize(":OK_hand:Играть!:red_exclamation_mark:"), callback_data=callback_puzzle.new(modle="puzzle", action="go")),

        types.InlineKeyboardButton(text=emoji.emojize(':sunset:Империя'),
                                   callback_data=callback_puzzle.new(modle="puzzle", action="situation")),

        types.InlineKeyboardButton(text=emoji.emojize(':information:Информация'), callback_data=callback_rules.new(modle="rules", action='view')),

        types.InlineKeyboardButton(text=emoji.emojize(':scroll:Указы'),
                                   callback_data=callback_puzzle.new(modle="puzzle", action="decrets")),
        types.InlineKeyboardButton(text=emoji.emojize(':currency_exchange:Биржа'),
                                   callback_data=callback_puzzle.new(modle="puzzle", action="exchange")),
        types.InlineKeyboardButton(text=emoji.emojize(':megaphone:Рекомендации'),
                                   callback_data=callback_puzzle.new(modle="puzzle", action="recomend")),
        types.InlineKeyboardButton(text=emoji.emojize(':briefcase:План на год'),
                                    callback_data=callback_puzzle.new(modle="main", action="view")),
        types.InlineKeyboardButton(text=emoji.emojize(':books:История'),
                                   callback_data=callback_puzzle.new(modle="puzzle", action="history")),

    ]

    # chat_id = int(message.chat.id) scroll information
    # certies = Eforia.select().where(Eforia.telega_id == chat_id)
    #
    # logger.info(len(certies))
    #
    # if len(certies) > 0:
    #     buttons.append(types.InlineKeyboardButton(text="Мои QR-коды",
    #                                               callback_data=callback_puzzle.new(modle="puzzle", action="my")))

    # logger.info(ADMINS)

    # if  str(message.chat.id) in ADMINS:
    #     buttons.append(types.InlineKeyboardButton(text="spam", callback_data=callback_admin.new(action="spam")))

    # Благодаря row_width=2, в первом ряду будет две кнопки, а оставшаяся одна
    # уйдёт на следующую строку
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    keyboard.add(*buttons)
    return keyboard

def keyb_menu(txt_list, rows=2):
    buttons = []
    for txt in txt_list:
        buttons.append(types.KeyboardButton(text=emoji.emojize(txt)))

    keyboard = types.ReplyKeyboardMarkup(row_width=rows)
    keyboard.add(*buttons)
    return keyboard
