from aiogram import types
from config import ADMINS

from loader import dp


def add_emo_menu(message):
    # Генерация menu.
    buttons = []

    if str(message.chat.id) in ADMINS:
        buttons.append(types.InlineKeyboardButton(text="Добавить еще", callback_data="add_emo"))
        buttons.append(types.InlineKeyboardButton(text="Все, хватит", callback_data="start"))

    # Благодаря row_width=2, в первом ряду будет две кнопки, а оставшаяся одна
    # уйдёт на следующую строку
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    keyboard.add(*buttons)
    return keyboard
