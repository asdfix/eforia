from .start import dp
from .help import dp
from .puzzle import dp
from .war import dp
from .decret import dp
from .exchange import dp
from .main import dp
from .recomend import dp
from .history import dp
from .rules import dp
from .admin import dp

__all__ = ["dp"]
