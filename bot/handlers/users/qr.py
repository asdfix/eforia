import datetime
import re
import aiofiles
import emoji
from aiofiles import os
from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.utils.markdown import hlink

from bot.utils.misc import logging
from config import IMG_PATH
from db.models import Eforia
# from bot.handlers.cbdata import callback_qr
from bot.keyboards.inline.main_menu import main_menu

from loader import dp

from bot.utils.misc.logging import logger
# from utils.utils import get_cert_id


@dp.callback_query_handler(callback_qr.filter(modle='qr', action="save"), state="*")
async def qr_go(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    UserProfile = await state.get_data()

    if 'certificate_data' in UserProfile:

        # UserProfile['certificate_data']
        logger.info(UserProfile['certificate_data'])

        cert_id = get_cert_id(UserProfile['certificate_data'])

        try:  # TODO check cert_id before load to base

            res = CertModel.insert({
                CertModel.telega_id: call.message.chat.id,
                CertModel.f: UserProfile['certificate_data']['f'],
                CertModel.i: UserProfile['certificate_data']['i'],
                CertModel.o: UserProfile['certificate_data']['o'],
                CertModel.pass_ser: UserProfile['certificate_data']['pass_ser'],
                CertModel.pass_no: UserProfile['certificate_data']['pass_no'],
                CertModel.birthday: UserProfile['certificate_data']['birthday'],
                CertModel.cert_id: cert_id,
                CertModel.region: UserProfile['certificate_data']['region'],
                CertModel.open: datetime.datetime.now()
            }).execute()
        except:
            txt = f'''
                Чота пашло не так!
            '''

            return await call.message.answer(emoji.emojize(txt), reply_markup=main_menu(call.message))

        # отправляем куаркод
        await call.message.answer_photo(photo=types.InputFile('{}/{}.png'.format(IMG_PATH, cert_id)))

        txt = ":red_exclamation_mark: Чтобы воспользоваться в любой момент QR-кодом, не забудь сохранить его себе на телефон :check_mark_button:"
        txt += "\n\nРасскажи друзьям: @QREmulatorBot"
        await call.message.answer(text=emoji.emojize(txt))

        utxt = "Отблагодарить за работу и помочь проекту можно "
        utxt += hlink('тут', 'https://pay.cloudtips.ru/p/d0ef58d2')
        utxt += "."
        return await call.message.answer(text=utxt, reply_markup=main_menu(call.message), disable_web_page_preview=True)
        # return call.message.answer(text='<a href="https://pay.cloudtips.ru/p/d0ef58d2">Поддержи наш проект!</a>', parse_mode="HTML")


@dp.callback_query_handler(callback_qr.filter(modle='qr', action="view"), state="*")
async def puzzle_view(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    logger.info(callback_data)

    cleared_input = re.match('([\w\d]{8}\-[\w\d]{4}\-[\w\d]{4}\-[\w\d]{4}\-[\w\d]{12})',
                             callback_data['cid'])

    try:
        cert_id = cleared_input.group(1)
        # logging.info(cert_id)
        certy = CertModel.get(CertModel.cert_id == cert_id)

        # отправляем куаркод
        await call.message.answer_photo(photo=types.InputFile('{}/{}.png'.format(IMG_PATH, cert_id)))


        txt = ":red_exclamation_mark: Чтобы воспользоваться в любой момент QR-кодом, не забудь сохранить его себе на телефон :check_mark_button:"
        txt += "\n\nРасскажи друзьям: @QREmulatorBot"
        await call.message.answer(text=emoji.emojize(txt))

        utxt = "Отблагодарить за работу и помочь проекту можно "
        utxt += hlink('тут', 'https://pay.cloudtips.ru/p/d0ef58d2')
        utxt += "."
        await call.message.answer(text=utxt, reply_markup=main_menu(call.message), disable_web_page_preview=True)

        return
        # return call.message.answer(text='<a href="https://pay.cloudtips.ru/p/d0ef58d2">Поддержи наш проект!</a>',parse_mode="HTML")



    except:

        return await call.message.answer(emoji.emojize("Не нашел в базе такого QR-кода. Можно создать новый."), reply_markup=main_menu(call.message))


@dp.callback_query_handler(callback_qr.filter(modle='qr', action="kill"), state="*")
async def puzzle_kill(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    cleared_input = re.match('([\w\d]{8}\-[\w\d]{4}\-[\w\d]{4}\-[\w\d]{4}\-[\w\d]{12})',
                             callback_data['cid'])

    try:
        cert_id = cleared_input.group(1)
        # logging.info(cert_id)
        certy = CertModel.get(CertModel.cert_id == cert_id)

        certy.delete_instance()
        fname = '{}/{}.png'.format(IMG_PATH, cert_id)

        # logger.info(fname)
        # async with aiofiles.open(fname, mode='r', encoding='UTF-8',  errors='strict', buffering=1) as f:
        #     os.remove(fname)

        await aiofiles.os.remove(fname) # kill png

        return await call.message.answer(emoji.emojize("OK!"), reply_markup=main_menu(call.message))


    except:
        return await call.message.answer(emoji.emojize("Не нашел в базе такого QR-кода. Может уже удалено. Можно создать новый"), reply_markup=main_menu(call.message))
