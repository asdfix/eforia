import emoji
from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.types import ReplyKeyboardRemove

from bot.states.decret_state import decret_state
from bot.utils.utils import sstr
from db.models import Eforia
from bot.handlers.cbdata import callback_puzzle, callback_button  #
from bot.keyboards.inline.main_menu import uni_menu, main_menu, keyb_menu
from loader import dp
from bot.states import puzzle_state
from bot.utils.misc.logging import logger


def decret_reply_markup():
    buttons = [

        (emoji.emojize(':tractor:На Посев'), callback_puzzle.new(modle="decret", action="decret2seed")),
        (emoji.emojize(':bread:На Еду'), callback_puzzle.new(modle="decret", action="decret2food")),

        (emoji.emojize(':sunset:Империя'), callback_puzzle.new(modle="puzzle", action="situation")),
        (emoji.emojize(':currency_exchange:Биржа'), callback_puzzle.new(modle="puzzle", action="exchange")),
        (emoji.emojize(':briefcase:План на год'), callback_puzzle.new(modle="main", action="view")),
    ]
    reply_markup = uni_menu(buttons)
    return reply_markup


@dp.callback_query_handler(callback_puzzle.filter(modle='decret', action="decret2food"), state="*")
async def decret2food(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    await call.message.delete_reply_markup()
    await call.answer(cache_time=60)

    chat_id = int(call.message.chat.id)
    imperia_data = Eforia.select().where(Eforia.telega_id == chat_id).get()

    opti = int(imperia_data.people * 40)
    opti1 = int(opti*1.1)
    opti9 = int(opti * .9)

    buttons = [
        (emoji.emojize(str(opti9)), callback_button.new(modle="decret", action="set2food", choice=str(opti9))),
        (emoji.emojize(str(opti)), callback_button.new(modle="decret", action="set2food", choice=str(opti))),
        (emoji.emojize(str(opti1)), callback_button.new(modle="decret", action="set2food", choice=str(opti1))),
    ]
    reply_markup = uni_menu(buttons, rows=3)

    await call.message.answer(text="Сколько прикажете оставить на еду подданным? (min " + str(opti) + " )", reply_markup=reply_markup)
    return await decret_state.waiting_food.set()

#button food
@dp.callback_query_handler(callback_button.filter(modle='decret', action="set2food"), state="*")
async def set2food(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    await call.message.delete_reply_markup()
    await call.answer(cache_time=60)

    txt = ""

    code = call.data.split(":")
    inp = int(code[3])
    logger.debug(code[3])

    chat_id = int(call.message.chat.id)
    imperia_data = Eforia.select().where(Eforia.telega_id == chat_id).get()

    if inp <= imperia_data.food + imperia_data.buy_food - imperia_data.sell_food:
        Eforia.update(decret2food=inp).where(Eforia.telega_id == chat_id).execute()
        txt += "Отправляю на еду :ear_of_corn:" + sstr(inp)
    else:
        txt = "Нет столько :ear_of_corn:"

    await call.message.answer(text=emoji.emojize(txt), reply_markup=decret_reply_markup())
    return await decret_state.start.set()

@dp.callback_query_handler(callback_puzzle.filter(modle='decret', action="decret2seed"), state="*")
async def decret2seed(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    await call.message.delete_reply_markup()
    await call.answer(cache_time=60)

    chat_id = int(call.message.chat.id)
    imperia_data = Eforia.select().where(Eforia.telega_id == chat_id).get()

    opti = min(imperia_data.people * 10, imperia_data.land)
    opti2 = max(imperia_data.people * 10, imperia_data.land)

    buttons = [
        (emoji.emojize(str(opti)), callback_button.new(modle="decret", action="set2seed", choice=str(opti))),
        (emoji.emojize(str(opti2)), callback_button.new(modle="decret", action="set2seed", choice=str(opti2))),
    ]
    reply_markup = uni_menu(buttons)
    # reply_markup = keyb_menu([str(opti)])

    await call.message.answer(text="Сколько прикажете засеять? (min " + str(opti) + " )", reply_markup=reply_markup)
    return await decret_state.waiting_seed.set()

#button seed
@dp.callback_query_handler(callback_button.filter(modle='decret', action="set2seed"), state="*")
async def set2seed(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    await call.message.delete_reply_markup()
    await call.answer(cache_time=60)

    txt = ""

    code = call.data.split(":")
    inp = int(code[3])
    logger.debug(code[3])

    chat_id = int(call.message.chat.id)
    imperia_data = Eforia.select().where(Eforia.telega_id == chat_id).get()


    if inp <= imperia_data.food + imperia_data.buy_food - imperia_data.sell_food:
        Eforia.update(decret2seed=inp).where(Eforia.telega_id == chat_id).execute()
        txt += "Отправляю на посевы :ear_of_corn:" + str(inp)
    else:
        txt = "Нет столько :ear_of_corn:"

    await call.message.answer(text=emoji.emojize(txt), reply_markup=decret_reply_markup())
    return await decret_state.start.set()



@dp.callback_query_handler(callback_puzzle.filter(modle='decret', action="decret2caravan"), state="*")
async def decret2caravan(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    await call.message.delete_reply_markup()
    await call.answer(cache_time=60)

    chat_id = int(call.message.chat.id)
    imperia_data = Eforia.select().where(Eforia.telega_id == chat_id).get()

    logger.debug(imperia_data.decret2caravan)
    logger.debug(imperia_data.p_caravan)

    if imperia_data.decret2caravan > 0:
        return await call.message.answer(text="Надо сначала дождаться предыдущего!")
    if imperia_data.p_caravan < 70:
        return await call.message.answer(text="Караван пока не собирается в дорогу")
    if imperia_data.buy_food > 0:
        return await call.message.answer(text="Караван отправляется до операций на бирже!")
    if imperia_data.buy_land > 0:
        return await call.message.answer(text="Караван отправляется до операций на бирже!")

    await call.message.answer(text="Какую сумму прикажете вложить в караван? (max " + str(imperia_data.money) + " )")
    return await decret_state.waiting_caravan.set()

@dp.callback_query_handler(callback_puzzle.filter(modle='decret', action="decret2army"), state="*")
async def decret2army(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    await call.message.delete_reply_markup()
    await call.answer(cache_time=60)

    chat_id = int(call.message.chat.id)
    imperia_data = Eforia.select().where(Eforia.telega_id == chat_id).get()

    logger.debug(imperia_data.decret2caravan)
    logger.debug(imperia_data.p_caravan)

    if imperia_data.buy_food > 0:
        return await call.message.answer(text="Нанять можно только до операций на бирже!")
    if imperia_data.buy_land > 0:
        return await call.message.answer(text="Нанять можно только до операций на бирже!")

    await call.message.answer(text="Сколько прикажете нанять наёмников?")
    return await decret_state.waiting_army.set()

@dp.callback_query_handler(callback_puzzle.filter(modle='decret', action="decret2del"), state="*")
async def decret2del(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    await call.message.delete_reply_markup()
    await call.answer(cache_time=60)

    await call.message.answer(text="Сколько прикажете выгнать наёмников?")
    return await decret_state.waiting_del.set()


@dp.message_handler(state=[
    decret_state.waiting_food,
    decret_state.waiting_seed,
    decret_state.waiting_caravan,
    decret_state.waiting_army,
    decret_state.waiting_del,
])
async def process_decret(message: types.Message, state: FSMContext):

    # await message.


    # await message.answer(text='', reply_markup=ReplyKeyboardRemove())

    inp = int(message.text)

    if inp > 0:
        chat_id = int(message.chat.id)
        imperia_data = Eforia.select().where(Eforia.telega_id == chat_id).get()

        state_info = await state.get_state()
        logger.info(state_info)
        txt = "ОК! "

        if state_info == 'decret_state:waiting_food': # на еду
            if inp <= imperia_data.food + imperia_data.buy_food - imperia_data.sell_food:
                Eforia.update(decret2food=inp).where(Eforia.telega_id == chat_id).execute()
                txt += "Отправляю на еду :ear_of_corn:" + sstr(inp)
            else:
                txt = "Нет столько :ear_of_corn:"

        elif state_info == 'decret_state:waiting_seed': # на посев
            if inp <= imperia_data.food + imperia_data.buy_food - imperia_data.sell_food:
                Eforia.update(decret2seed=inp).where(Eforia.telega_id == chat_id).execute()
                txt += "Отправляю на посевы :ear_of_corn:" + sstr(inp)
            else:
                txt = "Нет столько :ear_of_corn:"

        elif state_info == 'decret_state:waiting_caravan': # на караван
            if inp <= imperia_data.money:
                Eforia.update({
                    Eforia.decret2caravan:inp,
                    Eforia.p_caravan: 0,
                    Eforia.money: Eforia.money - inp,
                }).where(Eforia.telega_id == chat_id).execute()
                txt += "Отправляю с караваном :money_bag:" + sstr(inp)
            else:
                txt = "Нет столько :money_bag:. Есть всего " + sstr(imperia_data.money)

        elif state_info == 'decret_state:waiting_army': # на наемников
            if inp * 10 <= imperia_data.money:
                Eforia.update({
                    Eforia.decret2army: inp,
                    Eforia.money: Eforia.money - inp * 10,
                }).where(Eforia.telega_id == chat_id).execute()
                txt += "Нанимаю в армию :guard:" + sstr(inp)
            else:
                txt = "Нет столько :money_bag:. Могу нанять всего " + sstr(int(imperia_data.money / 10))

        elif state_info == 'decret_state:waiting_del': # на наемников
            if inp <= imperia_data.army:
                Eforia.update({
                    Eforia.army: Eforia.army - inp
                }).where(Eforia.telega_id == chat_id).execute()
                txt += "Выгоняю из армии :guard:" + sstr(inp)
            else:
                txt = "Нет столько :guard:. Могу выгнать всего " + sstr(int(imperia_data.army))

        buttons = [

            (emoji.emojize(':tractor:На Посев'), callback_puzzle.new(modle="decret", action="decret2seed")),
            (emoji.emojize(':bread:На Еду'), callback_puzzle.new(modle="decret", action="decret2food")),

            (emoji.emojize(':currency_exchange:Биржа'), callback_puzzle.new(modle="puzzle", action="exchange")),
            (emoji.emojize(':megaphone:Рекомендации'), callback_puzzle.new(modle="puzzle", action="recomend")),

            (emoji.emojize(':sunset:Империя'), callback_puzzle.new(modle="puzzle", action="situation")),
            (emoji.emojize(':briefcase:План на год'), callback_puzzle.new(modle="main", action="view")),
        ]
        reply_markup = uni_menu(buttons)
        await message.answer(text=emoji.emojize(txt), reply_markup=reply_markup)
        return await decret_state.start.set()
    else:
        return await message.answer(text="Не могу понять вас. Попробуйте снова")
