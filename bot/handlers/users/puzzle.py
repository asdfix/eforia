import random
import re

import emoji
import qrcode
from aiogram import types
from aiogram.dispatcher import FSMContext

from bot.utils.eforia_processor import EforiaProcessor
from bot.utils.utils import sstr, EXCH_COMM
from config import IMG_PATH

from db.models import Eforia
from bot.handlers.cbdata import callback_puzzle  #
from bot.keyboards.inline.main_menu import uni_menu, main_menu
from loader import dp
from bot.states import puzzle_state
from bot.utils.misc.logging import logger
# from bot.utils import simtext
# from bot.utils.utils import get_level_points, get_medal, get_next_puzzle
# from utils.utils import get_cert_id, get_qr_url

# logger.info('puzzle.py')


@dp.callback_query_handler(callback_puzzle.filter(modle='puzzle', action="situation"), state="*")
async def puzzle_situation(call: types.CallbackQuery, callback_data: dict, state: FSMContext):#Империя
    # telega_id: call.message.chat.id,
    # await call.message.delete_reply_markup()

    await call.answer(cache_time=60)

    EP = EforiaProcessor(call)

    need2eat = EP.data.people * 40
    need2seed = min(EP.data.people * 10, EP.data.land)
    need2army = EP.data.army * 100

    balance_food = EP.data.food - need2eat - need2seed - need2army

    txt = "Империя: " + str(EP.data.username) + "\n"
    txt += "------------------------------------------------\n"
    txt += ":calendar: " + str(EP.data.old) + "-й год правления " + EP.data.username + "\n"
    txt += ":family: " + sstr(EP.data.people) + "(:ear_of_corn: на еду: " + sstr(need2eat) + " )\n"
    txt += ":ear_of_corn: " + sstr(EP.data.food) + " (" + sstr(balance_food) + ")\n"
    txt += ":camping: " + sstr(EP.data.land) + "(:ear_of_corn: на посев: " + sstr(need2seed) + " )\n"
    txt += ":money_bag: " + sstr(EP.data.money) + "\n"
    txt += ":guard: " + sstr(EP.data.army) + "(:ear_of_corn: на еду: " + sstr(need2army) + " )\n"

    garmony = EP.garmony()


    logger.debug((garmony, 'garmony'))
    txt += "------------------------------------------------\n"
    txt += "Индекс гармонии развития империи: " + str(garmony) + "\n"




    txt += "\n"
    txt += "Поделись с другом @EforiaGodBot\n"
    return await call.message.answer(text=emoji.emojize(txt), reply_markup=main_menu(call.message))

@dp.callback_query_handler(callback_puzzle.filter(modle='puzzle', action="decrets"), state="*") #Указы
async def puzzle_decrets(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    await call.message.delete_reply_markup()

    await call.answer(cache_time=60)

    chat_id = int(call.message.chat.id)
    logger.debug(chat_id)
    imperia_data = Eforia.select().where(Eforia.telega_id == chat_id).get()
    # logger.debug(len(imperia_data))
    logger.debug(imperia_data)

    txt0 = "Указы: " + str(imperia_data.username) + "\n"
    txt = ""

    if imperia_data.decret2food:
        txt += ":ear_of_corn: отдать на еду для :family: " + sstr(imperia_data.decret2food) + "\n"
    if imperia_data.decret2seed:
        txt += ":ear_of_corn: отдать на посевную :camping: " + sstr(imperia_data.decret2seed) + "\n"
    if imperia_data.decret2caravan:
        txt += ":money_bag: снаряжён караван :camel: " + sstr(imperia_data.decret2caravan) + "\n"
    if imperia_data.decret2army:
        txt += ":money_bag: нанять наёмников :guard: " + sstr(imperia_data.decret2army) + "\n"


    if txt == "":
        txt = "Пока нет указов"


    buttons = [
        (emoji.emojize(':tractor:На Посев'), callback_puzzle.new(modle="decret", action="decret2seed")),
        (emoji.emojize(':bread:На Еду'), callback_puzzle.new(modle="decret", action="decret2food")),
        (emoji.emojize(':plus::guard:Нанять Наёмников'), callback_puzzle.new(modle="decret", action="decret2army")),
        (emoji.emojize(':minus::guard:Выгнать Наёмников'), callback_puzzle.new(modle="decret", action="decret2del")),
        (emoji.emojize(':sunset:Империя'), callback_puzzle.new(modle="puzzle", action="situation")),
        (emoji.emojize(':currency_exchange:Биржа'), callback_puzzle.new(modle="puzzle", action="exchange")),
    ]
    if imperia_data.decret2caravan == 0:
        if imperia_data.p_caravan > 70:
            if imperia_data.buy_food <= 0:
                if imperia_data.buy_land <= 0:
                    buttons.append((emoji.emojize(':camel:Снарядить караван!'), callback_puzzle.new(modle="decret", action="decret2caravan")))

    # (emoji.emojize(':sunset:Империя'), callback_puzzle.new(modle="puzzle", action="situation")),
    # (emoji.emojize(':currency_exchange:Биржа'), callback_puzzle.new(modle="puzzle", action="exchange")),
    # (emoji.emojize(':briefcase:План на год'), callback_puzzle.new(modle="main", action="view"))

    reply_markup = uni_menu(buttons)
    return await call.message.answer(text=emoji.emojize(txt0+txt), reply_markup=reply_markup)

@dp.callback_query_handler(callback_puzzle.filter(modle='puzzle', action="exchange"), state="*")
async def puzzle_exchange(call: types.CallbackQuery, callback_data: dict, state: FSMContext):#Биржа

    await call.message.delete_reply_markup()

    await call.answer(cache_time=60)

    chat_id = int(call.message.chat.id)
    logger.debug(chat_id)
    imperia_data = Eforia.select().where(Eforia.telega_id == chat_id).get()

    logger.debug(imperia_data)

    txt0 = "Ситуация на бирже:\n"
    txt0 += ":ear_of_corn: покупка/продажа: " + str(round((1 - EXCH_COMM) * imperia_data.food_rate/100,2)) + "/" + str(round((1 + EXCH_COMM) * imperia_data.food_rate/100,2)) + "\n"
    txt0 += ":camping: покупка/продажа: " + str(round((1 - EXCH_COMM) * imperia_data.land_rate/100,2)) + "/" + str(round((1 + EXCH_COMM) * imperia_data.land_rate/100,2)) + "\n"
    txt0 += "Брокеру от " + str(imperia_data.username) + ":\n"

    txt = ""
    if imperia_data.buy_food:
        txt += "купить :ear_of_corn: " + sstr(imperia_data.buy_food) + "\n"
    if imperia_data.sell_food:
        txt += "продать :ear_of_corn: " + sstr(imperia_data.sell_food) + "\n"
    if imperia_data.buy_land:
        txt += "купить :camping: " + sstr(imperia_data.buy_land) + "\n"
    if imperia_data.sell_land:
        txt += "продать :camping: " + sstr(imperia_data.sell_land) + "\n"

    if txt == "":
        txt = "Пока нет заявок"


    buttons = [

        (emoji.emojize('купить :ear_of_corn: '), callback_puzzle.new(modle="exchange", action="buy_food")),
        (emoji.emojize('продать :ear_of_corn: '), callback_puzzle.new(modle="exchange", action="sell_food")),
        (emoji.emojize('купить :camping: '), callback_puzzle.new(modle="exchange", action="buy_land")),
        (emoji.emojize('продать :camping: '), callback_puzzle.new(modle="exchange", action="sell_land")),


        (emoji.emojize(':sunset:Империя'), callback_puzzle.new(modle="puzzle", action="situation")),
        (emoji.emojize(':currency_exchange:Биржа'), callback_puzzle.new(modle="puzzle", action="exchange")),
        (emoji.emojize(':briefcase:План на год'), callback_puzzle.new(modle="main", action="view"))
    ]
    reply_markup = uni_menu(buttons)
    return await call.message.answer(text=emoji.emojize(txt0+txt), reply_markup=reply_markup)
