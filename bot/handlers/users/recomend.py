from aiogram import types

import emoji
from aiogram.dispatcher import FSMContext

from bot.handlers.cbdata import callback_puzzle
from bot.keyboards.inline.main_menu import main_menu
from bot.utils.eforia_processor import EforiaProcessor
from bot.utils.utils import sstr, EXCH_COMM
from loader import dp

@dp.callback_query_handler(callback_puzzle.filter(modle='puzzle', action="recomend"), state="*")
async def puzzle_recomend(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    # Анализ ситуации в империи
    await call.answer(cache_time=60)

    EP = EforiaProcessor(call)

    recom = "Рекомендуется:\n"

    txt = "Анализ положения дел в империи: " + str(EP.data.username) + "\n"
    txt += "------------------------------------------------\n"

    # зерно
    need2eat = EP.data.people * 40
    need2seed = min(EP.data.people * 10, EP.data.land)
    need2army = EP.data.army * 100
    need_food = need2eat + need2seed + need2army

    txt += ":ear_of_corn: потребуется: " + sstr(need_food) + "\n"
    txt += ":ear_of_corn: в наличии: " + sstr(EP.data.food) + "\n"
    if need_food > EP.data.food:
        txt += ":ear_of_corn: дефицит: " + sstr(need_food - EP.data.food) + "\n"
        recom += " к закупке минимум :ear_of_corn:" + sstr(need_food - EP.data.food) + " (:money_bag: -" + sstr(
            (need_food - EP.data.food) * round((1 + EXCH_COMM) * EP.data.food_rate / 100, 2)) + ")\n"
    else:
        txt += ":ear_of_corn: излишки: " + sstr(EP.data.food - need_food) + "\n"
        recom += " к продаже максимум :ear_of_corn:" + sstr(EP.data.food - need_food) + " (:money_bag: +" + sstr(
            (EP.data.food - need_food) * round((1 - EXCH_COMM) * EP.data.food_rate / 100, 2)) + ")\n"

    txt += "------\n"

    # земля
    txt += ":camping: Население может обработать: " + sstr(EP.data.people * 10) + "\n"
    txt += ":camping: в наличии: " + sstr(EP.data.land) + "\n"

    if EP.data.people * 10 > EP.data.land:
        txt += ":camping: дефицит: " + sstr(EP.data.people * 10 - EP.data.land) + "\n"
        recom += " к закупке минимум :camping:" + sstr(EP.data.people * 10 - EP.data.land) + " (:money_bag: -" + sstr(
            (EP.data.people * 10 - EP.data.land) * round((1 + EXCH_COMM) * EP.data.land_rate / 100, 2)) + ")\n"

    else:
        txt += ":camping: излишки: " + sstr(EP.data.land - EP.data.people * 10) + "\n"
        recom += " к продаже максимум :camping:" + sstr(EP.data.land - EP.data.people * 10) + " (:money_bag: +" + sstr(
            (EP.data.land - EP.data.people * 10) * round((1 - EXCH_COMM) * EP.data.land_rate / 100, 2)) + ")\n"

    txt += "------\n"

    # армия
    txt += "Спокойную жизнь населения должны обеспечить :guard:" + sstr(round(EP.data.people / 10)) + "\n"
    if EP.data.army > round(EP.data.people / 10):
        recom += "Можно распустить :guard: " + sstr(EP.data.army - round(EP.data.people / 10)) + "\n"
    else:
        recom += "Желательно нанять :guard: " + sstr(round(EP.data.people / 10) - EP.data.army) + "\n"
    txt += "------\n"

    txt += recom
    txt += "\n"
    ############################################
    # урожай
    if EP.data.wait_w_harvest > 1.11:
        txt += ":red_circle::red_exclamation_mark:Низкий урожай прошлого года породил продовольственный кризис! Враг у ворот!..."
    elif EP.data.wait_w_harvest > 1.07:
        txt += ":orange_circle::red_exclamation_mark:Голод неурожая прошлого года может спровоцировать войну..."
    elif EP.data.wait_w_harvest > 1.03:
        txt += ":yellow_circle::warning:Слухи о плохом урожае прошлого года достигли заграницы..."
    elif EP.data.wait_w_harvest > 1:
        txt += ":warning:Население не довольно низким урожаем прошлого года..."
    elif EP.data.wait_w_harvest > .85:
        txt += ":backhand_index_pointing_right:Урожай прошлого года позволяет быть населению в достатке..."
    else:
        txt += ":folded_hands:Население с прошлого года урожайного изобилия славит богов и мудрость правителя..."
    txt += "\n"
    ############################################
    if EP.data.p_war > 75:
        txt += ":red_circle::double_exclamation_mark:Напряженность на границах взрывоопасна!"
    elif EP.data.p_war > 50:
        txt += ":orange_circle::red_exclamation_mark:Напряженность на границах требует решительных мер!"
    elif EP.data.p_war > 25:
        txt += ":yellow_circle::red_exclamation_mark:Напряженность на границах вызывает озабоченность..."
    elif EP.data.p_war > 15:
        txt += ":warning:Сообщают о признаках напряженности на границах..."
    else:
        txt += ":green_circle:На границе всё спокойно..."



    return await call.message.answer(text=emoji.emojize(txt), reply_markup=main_menu(call.message))