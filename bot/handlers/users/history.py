import io
import emoji
import pandas as pd
import seaborn as sns
from aiogram import types
from aiogram.dispatcher import FSMContext

from bot.handlers.cbdata import callback_puzzle
from bot.keyboards.inline.main_menu import main_menu
from bot.utils.eforia_processor import EforiaProcessor
from bot.utils.misc.logging import logger
from config import IMG_PATH
from loader import dp
from PIL import Image
import matplotlib.pyplot as plt
import plotly.graph_objs as go
import plotly.io as pio
import kaleido


def fig2img(fig):
    """Convert a Matplotlib figure to a PIL Image and return it"""
    buf = io.BytesIO()
    fig.savefig(buf, format='JPEG')
    buf.seek(0)
    img = Image.open(buf)
    return img

@dp.callback_query_handler(callback_puzzle.filter(modle='puzzle', action="history"), state="*")
async def puzzle_recomend(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    # await call.answer(cache_time=60)

    await call.message.answer(text=emoji.emojize("Рисую..."))

    EP = EforiaProcessor(call)

    # logger.debug(EP.data.old)
    # logger.debug(EP.data.log_garmony)
    # logger.debug(EP.data.log_people)

    df = pd.DataFrame({
        'Год': range(EP.data.old),
        'Гармония': EP.data.log_garmony,
        'Население': EP.data.log_people,
        'Земли': EP.data.log_land,
        'Армия': EP.data.log_army,
        'Казна': EP.data.log_money,
    })
    df['Земли'] = df['Земли']/100
    df['Население'] = df['Население'] / 10
    df['Казна'] = df['Казна'] / 1000


    fig = go.Figure()
    fig.add_trace(go.Scatter(x=df['Год'], y=df['Гармония'], name='Гармония'))
    fig.add_trace(go.Scatter(x=df['Год'], y=df['Население'], name='Население/10'))
    fig.add_trace(go.Scatter(x=df['Год'], y=df['Земли'], name='Земли/100'))
    fig.add_trace(go.Scatter(x=df['Год'], y=df['Армия'], name='Армия'))
    fig.add_trace(go.Scatter(x=df['Год'], y=df['Казна'], name='Казна/1000'))

    fig.update_layout(legend_orientation="h",
                      legend=dict(x=.5, xanchor="center", orientation="h"),
                      title="История империи",
                      xaxis_title="Год правления",
                      yaxis_title="Показатель",
                      margin=dict(l=10, r=10, t=30, b=10)
                      )

    filename = '{}/{}.png'.format(IMG_PATH, EP.data.telega_id)
    pio.write_image(fig, filename)

    return await call.message.answer_photo(photo=types.InputFile(filename), reply_markup=main_menu(call.message))

