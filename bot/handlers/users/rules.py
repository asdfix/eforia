from aiogram import types
from aiogram.utils import emoji
from aiogram.utils.markdown import hlink

from bot.handlers.cbdata import callback_rules
from bot.keyboards.inline.main_menu import main_menu
from loader import dp


# @dp.callback_query_handler(text="rules")
@dp.callback_query_handler(callback_rules.filter(modle='rules', action='view'), state="*")
async def view_rules(call: types.CallbackQuery):
    # logger.info('puzzle_go')

    await call.answer(cache_time=60)

    txt = f'''
Игра. Стратегия. 
В память о БК-0010. 
Ты - правитель. Ресурсы: зерно, население, земля, деньги армия. 
Человек за год съедает 40 центнеров зерна. 
На посев уходит 1 центнер на га. 
Один человек обрабатывает 10 га земли. 
Есть биржа для продажи/покупки ресурсов. Караваны в далекие страны. 
Составляй Указы. Торгуй на бирже. Следи, чтобы не было войны.
Гармония и порядок - залог развития.
Управляй!

Начать заново - /start

Связь с админом - @EforiaHelperBot
'''

    txt += hlink('Помощь проекту', 'https://pay.cloudtips.ru/p/d0ef58d2')



    return await call.message.answer(emoji.emojize(txt), reply_markup=main_menu(call.message), disable_web_page_preview=True)
