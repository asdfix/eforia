import emoji
from aiogram import types
from aiogram.dispatcher import FSMContext

from bot.states.decret_state import decret_state
from bot.utils.utils import sstr, EXCH_COMM
from db.models import Eforia
from bot.handlers.cbdata import callback_puzzle
from bot.keyboards.inline.main_menu import uni_menu, main_menu
from loader import dp
from bot.states import puzzle_state, exchange_state
from bot.utils.misc.logging import logger

# ('купить :ear_of_corn: ', callback_puzzle.new(modle="decret", action="buy_food")),
# ('продать :ear_of_corn: ', callback_puzzle.new(modle="decret", action="buy_land")),
# ('купить :camping: ', callback_puzzle.new(modle="decret", action="sell_food")),
# ('продать :camping: ', callback_puzzle.new(modle="decret", action="sell_land")),

# waiting_buy_food = State()
# waiting_buy_land = State()
# waiting_sell_food = State()
# waiting_sell_land = State()

@dp.callback_query_handler(callback_puzzle.filter(modle='exchange', action="buy_food"), state="*")
async def buy_food(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    await call.answer(cache_time=60)
    state_info = await state.get_state()
    await call.message.answer(text="Сколько прикажете купить зерна?")
    return await exchange_state.waiting_buy_food.set()

@dp.callback_query_handler(callback_puzzle.filter(modle='exchange', action="buy_land"), state="*")
async def buy_land(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    await call.answer(cache_time=60)
    state_info = await state.get_state()
    await call.message.answer(text="Сколько прикажете купить земли?")
    return await exchange_state.waiting_buy_land.set()

@dp.callback_query_handler(callback_puzzle.filter(modle='exchange', action="sell_food"), state="*")
async def sell_food(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    await call.answer(cache_time=60)
    state_info = await state.get_state()
    await call.message.answer(text="Сколько прикажете продать зерна?")
    return await exchange_state.waiting_sell_food.set()

@dp.callback_query_handler(callback_puzzle.filter(modle='exchange', action="sell_land"), state="*")
async def sell_land(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    await call.answer(cache_time=60)
    state_info = await state.get_state()
    await call.message.answer(text="Сколько прикажете продать земли?")
    return await exchange_state.waiting_sell_land.set()

@dp.message_handler(state=[
    exchange_state.waiting_buy_food,
    exchange_state.waiting_buy_land,
    exchange_state.waiting_sell_food,
    exchange_state.waiting_sell_land
])
async def process_exchange(message: types.Message, state: FSMContext):


    inp = int(message.text)

    if inp > 0:
        chat_id = int(message.chat.id)
        imperia_data = Eforia.select().where(Eforia.telega_id == chat_id).get()

        state_info = await state.get_state()
        txt = "ОК!\n"

        money_after_exchange = imperia_data.money \
                               - imperia_data.buy_food * round((1 + EXCH_COMM) * imperia_data.food_rate / 100, 2) \
                               - imperia_data.buy_land * round((1 + EXCH_COMM) * imperia_data.land_rate / 100, 2) \
                               + imperia_data.sell_food * round((1 - EXCH_COMM) * imperia_data.food_rate / 100, 2) \
                               + imperia_data.sell_land * round((1 - EXCH_COMM) * imperia_data.land_rate / 100, 2)

        # logger.debug(money_after_exchange)
        # logger.debug(inp * round((1 + EXCH_COMM) * imperia_data.food_rate/100,2))

        if state_info == 'exchange_state:waiting_buy_food': # купить зерно
            if inp * round((1 + EXCH_COMM) * imperia_data.food_rate/100,2) <= money_after_exchange:
                txt += "Будет затрачено :money_bag:" + sstr(int(inp * round((1 + EXCH_COMM) * imperia_data.food_rate/100, 2)))
                Eforia.update(buy_food=inp).where(Eforia.telega_id == chat_id).execute()
            else:
                maybe = int(money_after_exchange / round((1 + EXCH_COMM) * imperia_data.food_rate/100,2))
                txt = "Нет столько :money_bag: Хватит только на " + str(maybe)

        elif state_info == 'exchange_state:waiting_buy_land': # купить землю
            if inp * round((1 + EXCH_COMM) * imperia_data.land_rate/100,2) <= money_after_exchange:
                txt += "Будет затрачено :money_bag:" + sstr(int(inp * round((1 + EXCH_COMM) * imperia_data.land_rate / 100, 2)))
                Eforia.update(buy_land=inp).where(Eforia.telega_id == chat_id).execute()
            else:
                maybe = int(money_after_exchange / round((1 + EXCH_COMM) * imperia_data.land_rate/100, 2))
                txt = "Нет столько :money_bag: Хватит только на " + str(maybe)

        elif state_info == 'exchange_state:waiting_sell_food': # продать зерно
            if inp <= imperia_data.food:
                txt += "Будет получено :money_bag:" + sstr(int(inp * round((1 - EXCH_COMM) * imperia_data.food_rate / 100, 2)))
                Eforia.update(sell_food=inp).where(Eforia.telega_id == chat_id).execute()
            else:
                txt = "Нет столько :ear_of_corn:. Есть всего " + str(imperia_data.food)

        elif state_info == 'exchange_state:waiting_sell_land': # продать землю
            if inp <= imperia_data.land:
                txt += "Будет получено :money_bag:" + sstr(int(inp * round((1 - EXCH_COMM) * imperia_data.land_rate / 100, 2)))
                Eforia.update(sell_land=inp).where(Eforia.telega_id == chat_id).execute()
            else:
                txt = "Нет столько :camping:. Есть всего " + str(imperia_data.land)

        buttons = [

            (emoji.emojize('купить :ear_of_corn: '), callback_puzzle.new(modle="exchange", action="buy_food")),
            (emoji.emojize('продать :ear_of_corn: '), callback_puzzle.new(modle="exchange", action="sell_food")),
            (emoji.emojize('купить :camping: '), callback_puzzle.new(modle="exchange", action="buy_land")),
            (emoji.emojize('продать :camping: '), callback_puzzle.new(modle="exchange", action="sell_land")),

            (emoji.emojize(':sunset:Империя'), callback_puzzle.new(modle="puzzle", action="situation")),
            (emoji.emojize(':briefcase:План на год'), callback_puzzle.new(modle="main", action="view")),
        ]
        reply_markup = uni_menu(buttons)
        await message.answer(text=emoji.emojize(txt), reply_markup=reply_markup)
        return await decret_state.start.set()
    else:
        return await message.answer(text="Не могу понять вас. Попробуйте снова")
