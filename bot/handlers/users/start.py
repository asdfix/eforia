import emoji
from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.builtin import CommandStart

# from bot.handlers.cbdata import callback_admin
from bot.keyboards.inline.main_menu import main_menu
from bot.states import puzzle_state
from db.models import Eforia
from loader import dp
from bot.utils.misc.logging import logger


# @dp.message_handler(state="*")
# async def bot_echo(message: types.Message):
#     logger.info('bot_echo')
#     txt = f'''
#         :waving_hand:Привет!:waving_hand:
#     Если хочешь, я помогу тебе сгенерировать тебе QR-код ссылки, которая приведет тебя на такую страницу, какая была бы у тебя, если бы ты сделал ненужную тебе прививку.
#     Все это бесплатно и быстро: всего после нескольких вопросов, без которых не обойтись.
#     Готов:red_question_mark:
#     Жми на кнопку!:backhand_index_pointing_down:
#     '''
#
#     return await message.answer(emoji.emojize(txt), reply_markup=main_menu(message))


@dp.message_handler(CommandStart(), state="*") # , state="*"
async def bot_start(message: types.Message, state: FSMContext):  # , state: FSMContext

    await state.reset_state()



    try:  # TODO check cert_id before load to base

        chat_id = int(message.chat.id)
        logger.debug(chat_id)

        Eforia.delete().where(Eforia.telega_id == chat_id).execute()

        imperia_data = Eforia.select().where(Eforia.telega_id == chat_id)

        logger.debug(len(imperia_data))
        logger.debug(message.chat.username)

        if len(imperia_data) <= 0:

            imperia_data = {
                Eforia.telega_id: chat_id,
                Eforia.old: 1,
                Eforia.people: 100,
                Eforia.food: 5000,
                Eforia.land: 1500,
                Eforia.money: 0,
                Eforia.army: 3,
                Eforia.p_war: 0,
                Eforia.p_hunger: 0,
                Eforia.p_caravan: 0,
                Eforia.username: str(message.chat.username),

                # Eforia.log_garmony: "0.25|",
                # Eforia.log_people: "100|",
                # Eforia.log_land: "1500|",
                # Eforia.log_army: "3|",
                # Eforia.log_money: "0|",
                # Eforia.log_event: "0|",
                #
                Eforia.log_garmony: "|".join(list(map(str, [.25]))),
                Eforia.log_people: "|".join(list(map(str, [100]))),
                Eforia.log_land: "|".join(list(map(str, [1500]))),
                Eforia.log_army: "|".join(list(map(str, [3]))),
                Eforia.log_money: "|".join(list(map(str, [0]))),
                Eforia.log_event: "|".join(list(map(str, [0]))),
            }
            logger.debug(imperia_data)
            res = Eforia.insert(imperia_data).execute()
    except:
        txt = f'''
            Чота пашло не так!
        '''
        logger.debug(txt)



    txt = f'''
    :waving_hand:Привет!:waving_hand:
    
Добро пожаловать в Эфорию!

Управляй поддаными. Будь справедливым и щедрым. А мы посмотрим, сколько ты продержишься.

Готов:red_question_mark:
Жми на ИМПЕРИЯ!:backhand_index_pointing_down:

Связь - @EforiaHelperBot
'''

    return await message.answer(emoji.emojize(txt), reply_markup=main_menu(message))
