import asyncio
import random

import emoji
from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.types import ChatActions

from bot.states.decret_state import decret_state
from bot.states.main_state import main_state
from bot.utils.eforia_processor import EforiaProcessor, differ
from bot.utils.utils import sstr
from db.models import Eforia
from bot.handlers.cbdata import callback_puzzle, callback_battle
from bot.keyboards.inline.main_menu import uni_menu, main_menu
from loader import dp
from bot.states import puzzle_state, battle_state
from bot.utils.misc.logging import logger

# main_state

@dp.callback_query_handler(callback_puzzle.filter(modle='main', action="view"), state="*")
async def main_view(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    ##############################################################################
    await call.message.delete_reply_markup()
    await call.answer(cache_time=60)
    # await bot.answer_callback_query(call.id)

    chat_id = int(call.message.chat.id)
    # logger.debug(chat_id)
    imperia_data = Eforia.select().where(Eforia.telega_id == chat_id).get()
    # logger.debug(len(imperia_data))
    logger.debug(imperia_data)

    txt0 = "Империя: " + str(imperia_data.username) + "\n"
    txt0 += ":calendar: " + str(imperia_data.old) + "\n"
    txt0 += ":family: " + sstr(imperia_data.people) + "\n"
    txt0 += ":ear_of_corn: " + sstr(imperia_data.food) + "\n"
    txt0 += ":camping: " + sstr(imperia_data.land) + "\n"
    txt0 += ":money_bag: " + sstr(imperia_data.money) + "\n"
    txt0 += ":guard: " + sstr(imperia_data.army) + "\n"

    txt0 += "Указы: " + str(imperia_data.username) + "\n"
    txt = ""



    if imperia_data.decret2food / 40 / (1+imperia_data.people) < .5:
        await call.message.answer(
            emoji.emojize(":red_circle::double_exclamation_mark:Очень мало еды! Будет голод!\n"))

    if imperia_data.decret2seed / imperia_data.land < .5:
        await call.message.answer(
            emoji.emojize(":red_circle::double_exclamation_mark:Надо увеличить посевы! Урожая может не хватить!\n"))

    if imperia_data.decret2food:
        txt += ":family: отдать на еду :ear_of_corn: " + sstr(imperia_data.decret2food) + "\n"
    if imperia_data.decret2seed:
        txt += ":camping: отдать на посевную :ear_of_corn: " + sstr(imperia_data.decret2seed) + "\n"
    if imperia_data.decret2caravan:
        txt += ":camel: снаряжён караван :coin: " + sstr(imperia_data.decret2caravan) + "\n"
    if imperia_data.decret2army:
        txt += ":guard: нанять наёмников :coin: " + sstr(imperia_data.decret2army) + "\n"

    if txt == "":
        txt = "Пока нет указов\n"

    txt0 += txt
    txt0 += "-------------------------\n"

    txt0 += "Брокеру от " + str(imperia_data.username) + ":\n"
    # txt0 += ":ear_of_corn: покупка/продажа: " + str(round(.9 * imperia_data.food_rate / 100, 2)) + "/" + str(
    #     round(1.1 * imperia_data.food_rate / 100, 2)) + "\n"
    # txt0 += ":camping: покупка/продажа: " + str(int(.9 * imperia_data.land_rate)) + "/" + str(
    #     int(1.1 * imperia_data.land_rate)) + "\n"
    txt = ""

    if imperia_data.buy_food:
        txt += "купить :ear_of_corn: " + sstr(imperia_data.buy_food) + "\n"
    if imperia_data.sell_food:
        txt += "продать :ear_of_corn: " + sstr(imperia_data.sell_food) + "\n"
    if imperia_data.buy_land:
        txt += "купить :camping: " + sstr(imperia_data.buy_land) + "\n"
    if imperia_data.sell_land:
        txt += "продать :camping: " + sstr(imperia_data.sell_land) + "\n"

    if txt == "":
        txt = "Пока нет заявок\n"

    txt0 += txt
    txt0 += "-------------------------\n"
    buttons = [
        (emoji.emojize(':sunset:Империя'), callback_puzzle.new(modle="puzzle", action="situation")),
        (emoji.emojize(':next_track_button:Исполнять!'), callback_puzzle.new(modle="main", action="go"))
    ]
    reply_markup = uni_menu(buttons)
    await call.message.answer(text=emoji.emojize(txt0), reply_markup=reply_markup)
    return await main_state.view.set()


@dp.callback_query_handler(callback_puzzle.filter(modle='main', action="go"), state=main_state.view)
async def main_go(call: types.CallbackQuery, callback_data: dict, state: FSMContext):
    await call.message.delete_reply_markup()
    await call.answer(cache_time=60)
    await call.message.answer(emoji.emojize("Исполняю....."), reply_markup=types.ReplyKeyboardRemove())


    ############################ ОСНОВНОЙ МОДУЛЬ #######################################
    EP = EforiaProcessor(call)

    EP.txt = ""
    EP.txt += "Итоги " + str(EP.data.old) + "-го года правления " + EP.data.username + "\n"
    EP.txt += "------------------------------------------------\n"

    await EP.start_year()

    if EP.p_war > .5:  # ВОЙНА

        # await call.message.answer(text=emoji.emojize(txt))

        txt2 = "------------------------------\n"
        txt2 += "ВОЙНА!\nОБЪЯВЛЕНА ВСЕОБЩАЯ МОБИЛИЗАЦИЯ!\n"
        txt2 += "------------------------------\n"

        # await EP.call.message.answer(emoji.emojize(txt2))
        await dp.bot.send_chat_action(EP.call.message.chat.id, ChatActions.TYPING)
        # await asyncio.sleep(1)

        EP.data.p_war = 0
        EP.p_lose = min(1, abs(1 - differ(1, EP.war_indicator)) * 4) * random.random()

        EP.battle_fields = []
        EP.selected_unit = []

        logger.debug(EP.war_indicator)

        eforia_data = EP.pack()

        logger.debug(eforia_data)

        buttons = [
            (emoji.emojize('Воевать!'), callback_battle.new(modle="war", action="start")),
            (emoji.emojize('АВТО'), callback_puzzle.new(modle="war", action="auto_war"))
        ]

        reply_markup = uni_menu(buttons)
        await battle_state.in_progress.set()
        return await call.message.answer(text=emoji.emojize(txt2), reply_markup=reply_markup)
    else:
        # await call.message.answer(text=emoji.emojize(txt), reply_markup=main_menu(call.message))
        await EP.finish_year()
        return await main_state.war.set()
