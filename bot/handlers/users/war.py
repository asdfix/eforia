import asyncio
import random

import emoji
from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.types import ChatActions

from bot.handlers.cbdata import callback_battle, callback_battle_unit
from bot.keyboards.inline.main_menu import main_menu
from bot.states import battle_state, main_state
from bot.utils.battle_processor import BattleProcessor
from bot.utils.eforia_processor import EforiaProcessor
from bot.utils.utils import edit_long_msg, sstr
from loader import dp
from bot.utils.misc.logging import logger

@dp.callback_query_handler(callback_battle.filter(modle='war', action="auto_war"), state=battle_state.in_progress)
async def main_auto_war(call: types.CallbackQuery, callback_data: dict, state: FSMContext):

    EP = EforiaProcessor(call)
    EP.unpack()
    Battle = BattleProcessor(EP)
    Battle.selected_unit = []
    logger.debug(Battle.fields)

    while not Battle.auto_move():

        Battle.txt += "-------------------------------\n"

        Battle.selected_unit = []
        EP.selected_unit = None
        EP.battle_txt = Battle.txt
        EP.pack()
        await call.message.edit_text(text=emoji.emojize(Battle.txt), reply_markup=Battle.view())
        # return await battle_state.select_unit.set()

    Battle.selected_unit = []
    EP.selected_unit = None
    EP.battle_txt = Battle.txt
    EP.pack()
    await call.message.edit_text(text=emoji.emojize(Battle.txt), reply_markup=Battle.view())

    if len(Battle.frag_units_coords) == 0:
        EP.war_indicator = 0  # ПОБЕДА!!!!!!
    # if len(Battle.my_units_coords) == 0: если битва завершилась не разгромом врага, то поражение
    else:
        EP.war_indicator = 1000000  # поражение!!!!!!

    await call.message.delete_reply_markup()

    xtx2 = await EP.war_result()
    await call.message.answer(emoji.emojize(xtx2))
    # await asyncio.sleep(1)
    EP.data.p_war=0
    await EP.finish_year()

    await battle_state.finished.set()
    return await main_state.start.set()

@dp.callback_query_handler(callback_battle.filter(modle='war', action="start"), state="*")
async def war_view(call: types.CallbackQuery, callback_data: dict, state: FSMContext): # стартовое размещение
    EP = EforiaProcessor(call)
    EP.unpack()

    Battle = BattleProcessor(EP)
    EP.battle_txt = Battle.txt
    EP.pack()

    await edit_long_msg(call, emoji.emojize(Battle.txt), Battle.view())

    if Battle.selected_unit:
        return await battle_state.move_unit.set()
    return await battle_state.select_unit.set()

@dp.callback_query_handler(callback_battle.filter(modle='war', action="pass"), state=[battle_state.select_unit, battle_state.in_progress])
async def unit_pass(call: types.CallbackQuery, state: FSMContext): #пропуск хода

    # await call.answer(cache_time=10)

    EP = EforiaProcessor(call)
    EP.unpack()
    Battle = BattleProcessor(EP)
    Battle.selected_unit = []
    logger.debug(Battle.fields)
    Battle.txt += "-------------------------------\n"

    if Battle.process_move():

        Battle.selected_unit = []
        EP.selected_unit = None
        EP.battle_txt = Battle.txt
        EP.pack()
        # await call.message.edit_text(text=emoji.emojize(Battle.txt), reply_markup=Battle.view())
        await edit_long_msg(call, emoji.emojize(Battle.txt), Battle.view())
        await battle_state.select_unit.set()

        await call.message.delete_reply_markup()

        if len(Battle.frag_units_coords) == 0:
            EP.war_indicator = 0  # ПОБЕДА!!!!!!
        # if len(Battle.my_units_coords) == 0: если битва завершилась не разгромом врага, то поражение
        else:
            EP.war_indicator = 1000000  # поражение!!!!!!

        xtx2 = await EP.war_result()
        await call.message.answer(emoji.emojize(xtx2))
        # await asyncio.sleep(1)
        EP.data.p_war = 0
        await EP.finish_year()
        return await main_state.start.set()

    Battle.selected_unit = []
    EP.selected_unit = None
    EP.battle_txt = Battle.txt
    EP.pack()
    await call.message.edit_text(text=emoji.emojize(Battle.txt), reply_markup=Battle.view())
    return await battle_state.select_unit.set()

@dp.callback_query_handler(callback_battle_unit.filter(modle='war', action="empty"), state=[battle_state.select_unit, battle_state.in_progress])
async def unit_select(call: types.CallbackQuery, state: FSMContext): #выбор юнита

    # await call.answer(cache_time=10)

    EP = EforiaProcessor(call)
    EP.unpack()
    Battle = BattleProcessor(EP)
    logger.debug(Battle.txt)


    callback_unit = call.data.split(":")[3]
    unit_coord = list(map(int, callback_unit.split("#"))) # (r,c) - координаты юнита
    logger.debug(unit_coord)

    logger.debug(Battle.fields[unit_coord[0]][unit_coord[1]])

    if Battle.fields[unit_coord[0]][unit_coord[1]] <=0:# не наш юнит
        # , show_alert=True
        return await call.answer(text=emoji.emojize("Это не твоё!"), show_alert=True)

    Battle.selected_unit = callback_unit
    EP.selected_unit = callback_unit
    EP.battle_txt = Battle.txt
    EP.pack()

    await call.message.edit_text(text=emoji.emojize(Battle.txt), reply_markup=Battle.view())
    return await battle_state.move_unit.set()

@dp.callback_query_handler(callback_battle.filter(modle='war', action="my_base"), state=battle_state.move_unit)
async def unit2my_base(call: types.CallbackQuery, state: FSMContext): #ход юнита на базу врага
    return await call.answer(text=emoji.emojize("Твой лагерь! Сюда нельзя допустить войска врога!"),
                             show_alert=True)

@dp.callback_query_handler(callback_battle.filter(modle='war', action="frag_base"), state=battle_state.move_unit)
async def unit2frag_base(call: types.CallbackQuery, state: FSMContext): #ход юнита на базу врага

    # await call.answer(cache_time=10)



    EP = EforiaProcessor(call)
    EP.unpack()
    Battle = BattleProcessor(EP)
    logger.debug(Battle.fields)

    logger.debug(call.data)


    # (r,c) - координаты активного юнита

    if Battle.selected_unit is None or "#" not in Battle.selected_unit:
        return await call.answer(text=emoji.emojize("Лагерь врага! Для победы сюда нужно привести свои войска!"),
                                 show_alert=True)

    unit_coord = list(map(int, Battle.selected_unit.split("#")))  # (r,c) - координаты юнита
    logger.debug(unit_coord)

    # проверка корректности хода
    if unit_coord[0] > 0:
        return await call.answer(text=emoji.emojize("Лагерь врага! Для победы сюда нужно привести свои войска!"),
                                 show_alert=True)

    Battle.txt += "-------------------------------\n"
    Battle.txt += str(int(abs(Battle.fields[unit_coord[0]][unit_coord[1]])))
    Battle.txt += ":triangular_flag:(r" + str(unit_coord[0] + 1) + "c" + str(unit_coord[1] + 1) + ")"
    Battle.txt += " захватывает вражескую базу.\n"
    Battle.txt += " конец битвы.\n"

    Battle.selected_unit = []
    EP.selected_unit = None
    EP.battle_txt = Battle.txt
    EP.pack()
    await call.message.edit_text(text=emoji.emojize(Battle.txt), reply_markup=Battle.view())
    await battle_state.select_unit.set()

    EP.war_indicator = 0 # ПОБЕДА!!!!!!
    xtx2 = await EP.war_result()
    await call.message.answer(emoji.emojize(xtx2))
    # await asyncio.sleep(1)
    EP.data.p_war = 0
    await EP.finish_year()
    await battle_state.finished.set()
    return await main_state.start.set()

@dp.callback_query_handler(callback_battle_unit.filter(modle='war', action="empty"), state=battle_state.move_unit)
async def unit_action(call: types.CallbackQuery, state: FSMContext): #ход юнита

    # await call.answer(cache_time=10)

    EP = EforiaProcessor(call)
    EP.unpack()
    Battle = BattleProcessor(EP)
    logger.debug(Battle.txt)

    callback_unit = call.data.split(":")[3]
    # (r,c) - координаты направления хода юнита
    move_coord = list(map(int, callback_unit.split("#")))
    logger.debug(move_coord)

    # (r,c) - координаты активного юнита
    unit_coord = list(map(int, Battle.selected_unit.split("#")))  # (r,c) - координаты юнита
    logger.debug(unit_coord)

    # проверка корректности хода
    dist = abs(move_coord[0] - unit_coord[0]) + abs(move_coord[1] - unit_coord[1])
    logger.debug(dist)
    if dist>1:
        return await call.answer(text=emoji.emojize("Ходим только на 1 клетку вперед/назад и влево/вправо!"), show_alert=True)

    if dist == 0: # отменяем выбранного юнита
        Battle.selected_unit = []
        EP.selected_unit = None
        EP.battle_txt = Battle.txt
        EP.pack()
        await call.message.edit_text(text=emoji.emojize(Battle.txt), reply_markup=Battle.view())
        return await battle_state.select_unit.set()

    # что на клетке хода?
    if Battle.fields[move_coord[0]][move_coord[1]] > 0: # наши
        Battle.fields[move_coord[0]][move_coord[1]] = Battle.fields[move_coord[0]][move_coord[1]] + Battle.fields[unit_coord[0]][unit_coord[1]]
        Battle.fields[unit_coord[0]][unit_coord[1]] = 0

    elif Battle.fields[move_coord[0]][move_coord[1]] < 0:  # враги, атакуем!!!

        return await call.answer(text=emoji.emojize("Чтобы занать место врага, его сначала нужно уничтожить"), show_alert=True)

    else: # empty
        Battle.fields[move_coord[0]][move_coord[1]] = Battle.fields[unit_coord[0]][unit_coord[1]]
        Battle.fields[unit_coord[0]][unit_coord[1]] = 0

    Battle.txt += "-------------------------------\n"
    Battle.txt += sstr(int(abs(Battle.fields[move_coord[0]][move_coord[1]])))
    Battle.txt += ":triangular_flag:(r" + str(unit_coord[0] + 1) + "c" + str(unit_coord[1] + 1) + ")"
    Battle.txt += " :right_arrow: (r"
    Battle.txt += str(move_coord[0] + 1) + "c" + str(move_coord[1] + 1) + ")\n"

    Battle.selected_unit = [move_coord[0], move_coord[1]]
    if Battle.process_move():

        # Battle.txt += ":triangular_flag:" + str(unit_coord[0] + 1) + "c" + str(unit_coord[1] + 1) + ")"
        # Battle.txt += " захватывает вражескую базу.\n"
        # Battle.txt += " конец битвы.\n"

        Battle.selected_unit = []
        EP.selected_unit = None
        EP.battle_txt = Battle.txt
        EP.pack()
        await call.message.edit_text(text=emoji.emojize(Battle.txt), reply_markup=Battle.view())
        await battle_state.select_unit.set()

        if len(Battle.frag_units_coords) == 0:
            EP.war_indicator = 0  # ПОБЕДА!!!!!!
        # if len(Battle.my_units_coords) == 0: если битва завершилась не разгромом врага, то поражение
        else:
            EP.war_indicator = 1000000  # поражение!!!!!!

        xtx2 = await EP.war_result()
        await call.message.answer(emoji.emojize(xtx2))
        # await asyncio.sleep(1)
        EP.data.p_war = 0
        await EP.finish_year()
        await battle_state.finished.set()
        return await main_state.start.set()

    Battle.selected_unit = []
    EP.selected_unit = None
    EP.battle_txt = Battle.txt
    EP.pack()
    await call.message.edit_text(text=emoji.emojize(Battle.txt), reply_markup=Battle.view())
    return await battle_state.select_unit.set()



    # идем, если есть свободное место потом бъем
# бъем всех по кругу (вперед, право, лево, назад) и получаем ответку
# первым вышел в тыл - победа
# три хода без атаки - -10%
#
#
