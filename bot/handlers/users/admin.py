import asyncio

import emoji
from aiogram import types
from aiogram.dispatcher import FSMContext

from bot.handlers.cbdata import callback_admin
from bot.states import admin_state
from config import ADMINS
from db.models import Eforia
from loader import dp
from bot.utils.misc.logging import logger

@dp.callback_query_handler(callback_admin.filter(action="spam"), state="*")
async def wait_spam(call: types.CallbackQuery, callback_data: dict, state: FSMContext):

    txt = "Готов принять сообщение для рассылки:backhand_index_pointing_down:"
    await call.message.answer(text=emoji.emojize(txt))
    await admin_state.waiting_spam.set()


@dp.message_handler(state=admin_state.waiting_spam)
async def preview_spam(message: types.Message, state: FSMContext):

    async with state.proxy() as data:
        logger.debug(data)

        txt = message.text

        data['spam'] = txt

        await message.answer(text=emoji.emojize(txt))
        reply_msg = await admin_state.preview_spam.set()


@dp.message_handler(state=admin_state.preview_spam)
async def go_spam(message: types.Message, state: FSMContext):



    async with state.proxy() as data:
        logger.debug('------')
        logger.debug(data)
        logger.debug('------')

        if message.text != data['spam']:
            txt = "Сообщение после корректуры не совпадает. Заново!:backhand_index_pointing_down:"
            await message.answer(text=emoji.emojize(txt))
            return await admin_state.waiting_spam.set()

        # спам готов к рассылке
        users = Eforia.select(Eforia.telega_id).distinct()

        logger.debug(ADMINS)

        for user in users:

            # if str(user.telega_id) not in ADMINS:
            #     continue

            logger.debug(user.telega_id)

            try:
                await dp.bot.send_message(chat_id=user.telega_id, text=message.text)
                await asyncio.sleep(1)
            except Exception as err:
                pass

        await message.answer(text=emoji.emojize('DONE.'))






