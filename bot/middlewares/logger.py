from aiogram import types
from aiogram.dispatcher import DEFAULT_RATE_LIMIT
from aiogram.dispatcher.middlewares import BaseMiddleware

from bot.utils.misc.logging import logger

class LoggingMiddleware(BaseMiddleware):
    """
    Simple middleware
    """

    def __init__(self, limit=DEFAULT_RATE_LIMIT, key_prefix='antiflood_'):
        self.rate_limit = limit
        self.prefix = key_prefix
        super(LoggingMiddleware, self).__init__()

    async def on_process_message(self, message: types.Message, data: dict):
        logger.info(message)

        states = await data['state'].get_state()
        logger.info(states)
        logger.info(data['command'])
