from aiogram import types

from bot.utils.misc.logging import logger

EXCH_COMM = .1

def sstr(txt: object) -> object:
    if type(txt) == int or type(txt) == float :
        if txt <= -10000000:
            return str(round(txt/1000000,2))+'kk'
        elif txt <= -10000:
            return str(round(txt/1000,2))+'k'
        elif txt >= 10000000:
            return str(round(txt/1000000,2))+'kk'
        elif txt >= 10000:
            return str(round(txt/1000,2))+'k'

    return str(int(txt))


async def edit_long_msg(call: types.CallbackQuery, text, reply_markup):

    def smart_split(txt):
        if len(txt) < 2048:
            return [txt,'']

        splitter = '\n'

        first = txt[:2048]
        txt_ara = first.split(splitter)

        txt2ret = txt_ara[-1] + txt[2048:]
        del txt_ara[-1]
        txt2send = splitter.join(txt_ara)
        return txt2send, txt2ret

    logger.debug(text)
    logger.debug(len(text))

    if len(text) > 2048:

        txt2send, txt2ret = smart_split(text)

        logger.debug(txt2send)
        logger.debug(len(txt2send))

        logger.debug(txt2ret)
        logger.debug(len(txt2ret))

        await call.message.answer(text=txt2send)

        await call.message.answer(text=txt2send, reply_markup=reply_markup)
        return txt2ret
    else:
        await call.message.edit_text(text=text, reply_markup=reply_markup)
        return text
