import logging

from aiogram import Dispatcher

from config import ADMINS


# from  import logger


async def on_startup_notify(dp: Dispatcher):
    return

    for admin in ADMINS:
        try:
            await dp.bot.send_message(admin, "Бот Запущен")

        except Exception as err:
            logging.exception(err)
