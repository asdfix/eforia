import random

import emoji
from aiogram import types

from bot.handlers.cbdata import callback_battle, callback_puzzle, callback_battle_unit

from bot.utils.misc.logging import logger
from bot.utils.utils import sstr


def sign(x):
    ret = 0 if not x else int(x / abs(x))
    return ret

class BattleProcessor():

    fields = []
    selected_unit = []
    txt = 'СРАЖЕНИЕ\n'# ......Обязательно почитай инструкцию о том, как воевать!

    def __init__(self, EP):

        if hasattr(EP, 'battle_fields'):
            if type(EP.battle_fields) == list:
                if EP.battle_fields:
                    self.fields = EP.battle_fields
                    if hasattr(EP, 'selected_unit'):
                        self.selected_unit = EP.selected_unit
                    if hasattr(EP, 'battle_txt'):
                        self.txt = EP.battle_txt

                    return

        my_army = EP.army_after * 1
        frag_army = int(my_army * EP.war_indicator)

        self.txt += 10 * ' :crossed_swords: '
        self.txt += '\n\n'
        self.txt += sstr(my_army) + ":triangular_flag:"
        self.txt += ' :VS_button: '
        self.txt += sstr(frag_army) + ":pirate_flag:\n"

        logger.debug(my_army)
        logger.debug(frag_army)

        my_units = []
        for i in range(4):
            my_units.append(int((my_army - sum(my_units))/2 * random.random()))
        my_units.append(int(my_army - sum(my_units)))
        random.shuffle(my_units)

        frag_units = []
        for i in range(4):
            frag_units.append(int((frag_army - sum(frag_units))/2 * random.random()))
        frag_units.append(int(frag_army - sum(frag_units)))
        frag_units = [-x for x in frag_units]
        random.shuffle(frag_units)

        # logger.debug(my_units)
        # logger.debug(frag_units)

        rows = [0,0,0,0,0]
        self.fields = [frag_units,rows,rows,rows,my_units]
        self.selected_unit = []

        # logger.debug(self.fields)
        # logger.debug(self.fields[0][4])
        EP.battle_fields = self.fields
        EP.pack()
        return

    def target(self, attack, defence, power=1):

        if int(abs(self.fields[attack[0]][attack[1]])) <=0:
            return

        logger.debug(('attack', attack))
        logger.debug(('defence', defence))
        logger.debug(('power', power))

        impact = int(abs(self.fields[attack[0]][attack[1]]) * power)
        logger.debug(('impact', impact))
        died = min(abs(self.fields[defence[0]][defence[1]]), int(impact * random.random()))
        logger.debug(('died', died))


        txt = ""
        # attaker
        txt += sstr(int(abs(self.fields[attack[0]][attack[1]])))
        txt += ":triangular_flag:" if self.fields[attack[0]][attack[1]] > 0 else ":pirate_flag:"
        # attaker coord
        txt += "(r" + str(attack[0]+1) + "c" + str(attack[1]+1) + ")"
        # has
        txt += ' :VS_button: '
        # defender
        txt += sstr(int(abs(self.fields[defence[0]][defence[1]])))
        txt += ":triangular_flag:" if self.fields[attack[0]][attack[1]] < 0 else ":pirate_flag:"
        # defender coord
        txt += "(r" + str(defence[0]+1) + "c" + str(defence[1]+1) + ")"
        # impact
        txt += ' :right-facing_fist: :crossed_swords:' + sstr(impact)

        # result
        # dead
        txt += ' :SOON_arrow: :skull_and_crossbones:'+ sstr(died)+ ' осталось '
        # defender
        txt += sstr(int(abs(self.fields[defence[0]][defence[1]]))-died)
        txt += ":triangular_flag:" if self.fields[attack[0]][attack[1]] < 0 else ":pirate_flag:"
        # defender coord
        txt += "(r" + str(defence[0] + 1) + "c" + str(defence[1] + 1) + ")"
        txt += '\n'
        logger.debug(txt)
        self.txt += txt

        # ответка
        impact0 = int(abs(self.fields[defence[0]][defence[1]]))
        logger.debug(('impact0', impact0))
        died0 = min(abs(self.fields[attack[0]][attack[1]]), int(impact0 * random.random()))
        logger.debug(('died0', died0))

        txt = ' в ответ '
        # attaker
        txt += str(int(abs(self.fields[defence[0]][defence[1]])))
        txt += ":triangular_flag:" if self.fields[defence[0]][defence[1]] > 0 else ":pirate_flag:"
        # attaker coord
        txt += "(r" + str(defence[0]+1) + "c" + str(defence[1]+1) + ")"
        # has
        txt += ' :VS_button: '
        # defender
        txt += str(int(abs(self.fields[attack[0]][attack[1]])))
        txt += ":triangular_flag:" if self.fields[defence[0]][defence[1]] < 0 else ":pirate_flag:"
        # defender coord
        txt += "(r" + str(attack[0]+1) + "c" + str(attack[1]+1) + ")"
        # impact
        txt += ' :right-facing_fist: :crossed_swords:' + sstr(impact0)

        # result
        # dead
        txt += ' :SOON_arrow: :skull_and_crossbones:'+ sstr(died0)+ ' осталось '
        # defender
        txt += str(int(abs(self.fields[attack[0]][attack[1]]))-died0)
        txt += ":triangular_flag:" if self.fields[defence[0]][defence[1]] < 0 else ":pirate_flag:"
        # defender coord
        txt += "(r" + str(attack[0] + 1) + "c" + str(attack[1] + 1) + ")"        
        txt += '\n'
        logger.debug(txt)
        self.txt += txt


        # потери
        if self.fields[defence[0]][defence[1]]>0:
            self.fields[defence[0]][defence[1]] = int(self.fields[defence[0]][defence[1]] - died)
        else:
            self.fields[defence[0]][defence[1]] = int(self.fields[defence[0]][defence[1]] + died)

        if self.fields[attack[0]][attack[1]]>0:
            self.fields[attack[0]][attack[1]] = int(self.fields[attack[0]][attack[1]] - died0)
        else:
            self.fields[attack[0]][attack[1]] = int(self.fields[attack[0]][attack[1]] + died0)

        if self.fields[defence[0]][defence[1]] == 0:
            if defence in self.my_units_coords:
                self.my_units_coords.remove(defence)
            if defence in self.frag_units_coords:
                self.frag_units_coords.remove(defence)
                
        if self.fields[attack[0]][attack[1]] == 0:
            if attack in self.my_units_coords:
                self.my_units_coords.remove(attack)
            if attack in self.frag_units_coords:
                self.frag_units_coords.remove(attack)

    def unit_round(self, unit_coord, power=1):

        # logger.debug(("раунд юнита", unit_coord))

        # есть ли враг впереди self.fields[unit_coord[0]][unit_coord[1]]

        u0 = unit_coord[0]
        u1 = unit_coord[1]

        # logger.debug("есть ли враг впереди")
        if u0 > 0:
            # logger.debug(sign(self.fields[u0 - 1][u1]))
            # logger.debug(sign(self.fields[u0][u1]))
            if sign(self.fields[u0 - 1][u1]) + sign(self.fields[u0][u1]) == 0:
                self.target([u0,u1], [u0 - 1,u1], power=power)

        # есть ли враг сзади
        # logger.debug("есть ли враг сзади")
        if u0 < 4:
            if sign(self.fields[u0 + 1][u1]) + sign(self.fields[u0][u1]) == 0:
                self.target([u0,u1], [u0 + 1,u1], power=power)

        # есть ли враг справа
        # logger.debug("есть ли враг справа")
        if u1 > 0:
            if sign(self.fields[u0][u1 - 1]) + sign(self.fields[u0][u1]) == 0:
                self.target([u0,u1], [u0,u1 - 1], power=power)

        # есть ли враг слева
        # logger.debug("есть ли враг слева")
        if u1 < 4:
            if sign(self.fields[u0][u1 + 1]) + sign(self.fields[u0][u1]) == 0:
                self.target([u0,u1], [u0,u1 + 1], power=power)

    def auto_move(self):
        logger.debug("***************auto_move ход***************")
        self.auto_my_unit_select()
        move_coord = self.selected_unit

        if len(move_coord) == 2:

            self.txt += sstr(int(abs(self.fields[move_coord[0]][move_coord[1]])))
            self.txt += ":triangular_flag:(" + str(move_coord[0] + 1) + "c" + str(move_coord[1] + 1) + ")"
            self.txt += " :right_arrow: (r"

            self.fields[move_coord[0]-1][move_coord[1]] = self.fields[move_coord[0]][move_coord[1]]
            self.fields[move_coord[0]][move_coord[1]] = 0

            self.my_units_coords.append(move_coord)
            self.txt += str(move_coord[0]) + "c" + str(move_coord[1] + 1) + ")\n"

            self.my_units_coords.remove(move_coord)
            self.my_units_coords.append([move_coord[0]-1, move_coord[1]])

        
        return self.process_move()

    def auto_my_unit_select(self):
        logger.debug("auto_my_unit_select")

        self.set_unit_coords()

        self.selected_unit = []

        free = []
        for сссс in self.my_units_coords:
            if сссс[0] == 0 or self.fields[сссс[0] - 1][сссс[1]] == 0:
                free.append(сссс)

        if len(free) > 0:

            self.selected_unit = random.choice(free)
            # logger.debug(my_coord)
            # # двигаем его вперед
            # if my_coord[0] == 4:
            #     # Враг победил
            #     self.txt += str(int(abs(self.fields[my_coord[0]][my_coord[1]])))
            #     self.txt += ":pirate_flag:(r" + str(my_coord[0] + 1) + "c" + str(my_coord[1] + 1) + ")"
            #     self.txt += " захватывает твою базу.\n"
            #     self.txt += " конец битвы.\n"
            #     return True

    def set_unit_coords(self):
        self.my_units_coords = []
        self.frag_units_coords = []

        r = 0
        for row in self.fields:
            c = 0
            for unit_field in row:
                if unit_field > 0:
                    self.my_units_coords.append([r, c])
                if unit_field < 0:
                    self.frag_units_coords.append([r, c])
                c += 1
            r += 1

    def process_move(self):

        self.set_unit_coords()

        logger.debug(self.my_units_coords)
        logger.debug(self.frag_units_coords)

        # мой ход
        logger.debug("мой ход")
        if self.selected_unit:
            # unit_coord = self.selected_unit  # (r,c) - координаты юнита
            self.unit_round(self.selected_unit, power=2)

        # ход врага
        logger.debug("ход врага")
        # выбираем тех, кому есть куда ходить
        free = []
        for сссс in self.frag_units_coords:
            if сссс[0]==4 or self.fields[сссс[0] + 1][сссс[1]] == 0:
                free.append(сссс)

        frag_coord = [] # этот ходит

        if len(free) > 0:

            frag_coord = random.choice(free) # случайнй ход врага

            # определяем вражеского юнита для хода по степени слабости противника
            frag_unit_power = []

            for i in range(5):
                power = 0
                for j in range(5):
                    power+=self.fields[j][i]
                frag_unit_power.append(power)

            logger.debug(frag_unit_power)

            frag_unit_power = [(x-min(frag_unit_power)) * random.random() for x in frag_unit_power]
            logger.debug(frag_unit_power)
            col_num = frag_unit_power.index(min(frag_unit_power))
            logger.debug(col_num)

            for candicate in free:
                if candicate[1] == col_num:
                    frag_coord = candicate

            if frag_coord:

                logger.debug(frag_coord)
                # двигаем его вперед
                if frag_coord[0] == 4:
                    # Враг победил
                    self.txt += sstr(int(abs(self.fields[frag_coord[0]][frag_coord[1]])))
                    self.txt += ":pirate_flag:(r" + str(frag_coord[0] + 1) + "c" + str(frag_coord[1] + 1) + ")"
                    self.txt += " захватывает твою базу.\n"
                    self.txt += " конец битвы.\n"
                    return True

                # двигаем врага вперед
                self.txt += sstr(int(abs(self.fields[frag_coord[0]][frag_coord[1]])))
                self.txt += ":pirate_flag:(" + str(frag_coord[0] + 1) + "c" + str(frag_coord[1] + 1) + ")"
                self.txt += " :right_arrow: (r"

                self.fields[frag_coord[0] + 1][frag_coord[1]] = self.fields[frag_coord[0]][frag_coord[1]]
                self.fields[frag_coord[0]][frag_coord[1]] = 0

                self.frag_units_coords.remove(frag_coord)
                frag_coord = [frag_coord[0]+1, frag_coord[1]]

                self.frag_units_coords.append(frag_coord)
                self.txt += str(frag_coord[0] + 1) + "c" + str(frag_coord[1] + 1) + ")\n"



                logger.debug(self.frag_units_coords)

                self.unit_round(frag_coord, power=2)
            else:
                logger.debug("Неокому ходить. Скип")
        else:
            logger.debug("Неокому ходить")

        # свалка
        logger.debug("свалка")
        coords = [*self.frag_units_coords, *self.my_units_coords]
        
        if self.selected_unit in coords:
            coords.remove(self.selected_unit)
        if frag_coord in coords:
            coords.remove(frag_coord)

        random.shuffle(coords)
        for ccc in coords: self.unit_round(ccc)

        if len(self.frag_units_coords)==0:
            self.txt += " Войско :pirate_flag: уничтожено.Победа за :triangular_flag:.\n"
            self.txt += " Конец битвы.\n"
            return True
        if len(self.my_units_coords)==0:
            self.txt += " Войско :triangular_flag: уничтожено.Победа за :pirate_flag:.\n"
            self.txt += " Конец битвы.\n"
            return True


        return False

    def view(self):

        keyboard = types.InlineKeyboardMarkup(row_width=5)

        my_base_button = types.KeyboardButton(text=emoji.emojize(':triangular_flag:'), callback_data=callback_battle.new(modle="war", action="my_base"))
        frag_base_button = types.KeyboardButton(text=emoji.emojize(':pirate_flag:'), callback_data=callback_battle.new(modle="war", action="frag_base"))

        keyboard.add(frag_base_button) # row=-1

        if self.selected_unit:
            unit_coord = list(map(int, self.selected_unit.split("#")))  # (r,c) - координаты юнита
            logger.debug(unit_coord)

        r=0
        for row in self.fields:
            c = 0
            for unit_field in row:
                if unit_field > 0:
                    txt = ':triangular_flag:' + sstr(unit_field)
                    if self.selected_unit:
                        if r==unit_coord[0]:
                            if c==unit_coord[1]:
                                logger.debug(unit_coord)
                                txt = "[" + txt + "]"

                elif unit_field < 0:
                    txt = ':pirate_flag:' + sstr(abs(unit_field))
                else:
                    txt = '___'

                unit = "#".join(map(str,[r,c])) # индекс кнопки на поляне

                # logger.debug((r,c,txt))

                button = types.KeyboardButton(text=emoji.emojize(txt), callback_data=callback_battle_unit.new(modle="war", action="empty", unit=unit))
                if c==0:
                    keyboard.add(button)
                else:
                    keyboard.insert(button)
                c+=1
            r+=1

        keyboard.add(my_base_button) # row=5

        buttons = [
            # types.KeyboardButton(text=emoji.emojize('Воевать!'), callback_data=callback_battle.new(modle="war", action="start")),
            types.KeyboardButton(text=emoji.emojize('АВТО'), callback_data=callback_puzzle.new(modle="war", action="auto_war")),
            types.KeyboardButton(text=emoji.emojize('SKIP'),
                                 callback_data=callback_battle.new(modle="war", action="pass"))
        ]
        keyboard.add(*buttons)
        return keyboard