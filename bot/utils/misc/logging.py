import logging

from config import *

logging_level = logging.DEBUG
if LOG_LEVEL == 'ERROR' : logging_level = logging.ERROR
elif LOG_LEVEL == 'INFO' : logging_level = logging.INFO
else: logging_level = logging.DEBUG

logging.basicConfig(format=u'%(filename)s [LINE:%(lineno)d] #%(levelname)-8s [%(asctime)s]  %(message)s',
                    filename=LOG_PATH + "/qr.log",
                    level=logging_level,  # Можно заменить на другой уровень логгирования.
                    )

console = logging.StreamHandler()
console.setLevel(logging_level)
formatter = logging.Formatter(u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)
logger = logging.getLogger(__name__)
