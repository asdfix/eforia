import asyncio
import json
import pickle
import random
import math
import emoji
from aiogram.types import ChatActions

from bot.keyboards.inline.main_menu import main_menu
from bot.utils.utils import sstr, EXCH_COMM
from db.models import Eforia
from bot.utils.misc.logging import logger
from loader import dp


def differ(x,y):
  return 1 + math.tanh((x+1)/(y+1)-1)

class EforiaProcessor():
    def __init__(self, call):
        self.call = call
        chat_id = int(call.message.chat.id)
        # logger.debug(chat_id)
        self.chat_id = chat_id
        self.data = Eforia.get(Eforia.telega_id == self.chat_id)

        if not self.data.username:
            self.data.username = 'Император_' + str(self.data.telega_id)

        self.data.log_garmony = list(map(float, self.data.log_garmony.split("|"))) if self.data.log_garmony else []
        self.data.log_people = list(map(int, self.data.log_people.split("|"))) if self.data.log_people else []
        self.data.log_land = list(map(int, self.data.log_land.split("|"))) if  self.data.log_land else []
        self.data.log_army = list(map(int, self.data.log_army.split("|"))) if self.data.log_army else []
        self.data.log_money = list(map(int, self.data.log_money.split("|"))) if self.data.log_money else []
        self.data.log_event = self.data.log_event.split("|") if self.data.log_event else []

    def pack(self):

        tmp = vars(self.data)
        # logger.debug(tmp)
        # logger.debug(type(tmp))
        # logger.debug(tmp['__data__'])

        ret = vars(self).copy()
        del ret['call']
        del ret['chat_id']
        ret['data'] = {key: value for key, value in tmp['__data__'].items()}
        if 'data' in ret:
            if 'cookie' in ret['data']:
                del ret['data']['cookie']

        if 'cookie' in ret:
            del ret['cookie']

        ret = json.dumps(ret)
        # ret = ret.replace(":","|")

        updated = {
            Eforia.cookie: ret
        }

        Eforia.update(updated).where(Eforia.telega_id == self.chat_id).execute()

        # logger.debug(ret)
        return ret

    def unpack(self):
        # eforia_data = eforia_data.replace("|", ":")
        for key, value in json.loads(self.data.cookie.replace("\'", '"')).items():
            if key!='data':
                # logger.debug((key, value))
                setattr(self, key, value)
            elif key=='data':
                for k,v in value.items():
                    # logger.debug((k,v))
                    if k not in ['cookie','txt']:
                        setattr(self.data, key, value)


    async def start_year(self):
        await self.random_tip()
        await self.process_exchange()
        self.txt += self.process_food()
        self.txt += self.process_people()

        await self.random_news()
        await self.random_news()

        self.txt += await self.process_harvest()

        await dp.bot.send_chat_action(self.call.message.chat.id, ChatActions.TYPING)
        # await asyncio.sleep(1)

        self.txt += await self.process_war()

    async def finish_year(self):
        self.txt += await self.process_caravan()

        await dp.bot.send_chat_action(self.call.message.chat.id, ChatActions.TYPING)
        # await asyncio.sleep(1)

        self.upsert()



        # logger.debug(EP.data.people)

        if self.data.people < 50:
            await self.call.message.answer(emoji.emojize(self.txt))


            txt = "Огромное число лишений и утрат сподвигло население поднять бунт!\n"
            txt += "Ваше правительство низложено.\n"
            txt += "Вас свергли и выгнали с позором.\n"
            txt += "Предлагаем начать игру с начала. Нажмите /start"

            return await self.call.message.answer(emoji.emojize(txt))
        else:
            await self.call.message.answer(emoji.emojize(self.txt), reply_markup=main_menu(self.call.message))
        
    async def process_exchange(self):
        txt = ""

        # биржа
        # land
        self.land_before = self.data.land
        self.land_for_seed = self.land_before + self.data.buy_land - self.data.sell_land
        # logger.debug(land_for_seed)

        # food
        self.food_before = self.data.food
        self.food_before_seed = self.food_before + self.data.buy_food - self.data.sell_food

        # котировки
        self.drate_land = max((1 - EXCH_COMM), min((1 + EXCH_COMM), (self.data.land_vol + self.data.buy_land - self.data.sell_land) / self.data.land_vol))
        self.drate_food = max((1 - EXCH_COMM), min((1 + EXCH_COMM), (self.data.food_vol + self.data.buy_food - self.data.sell_food) / self.data.food_vol))

        logger.debug(self.drate_land)
        logger.debug(self.drate_food)

        logger.debug(self.data.land_rate)
        logger.debug(self.data.food_rate)

        self.land_rate_after = min(50000, max(2,self.data.land_rate / self.drate_food * self.drate_land))
        self.food_rate_after = min(50000, max(2,self.data.food_rate * self.drate_food / self.drate_land))

        # self.land_rate_after = max(self.data.land_rate * self.drate_land / self.drate_food, 10)
        # self.food_rate_after = max(self.data.food_rate * self.drate_food / self.drate_land, 2)

        # self.land_rate_after = max(1, int(self.land_rate_after))
        # self.food_rate_after = max(1, int(self.food_rate_after))

        logger.debug(self.land_rate_after)
        logger.debug(self.food_rate_after)

        # money
        self.money_before = self.data.money
        self.money_after_exchange = self.money_before \
                               - self.data.buy_food * round((1 + EXCH_COMM) * self.data.food_rate / 100, 2) \
                               - self.data.buy_land * round((1 + EXCH_COMM) * self.data.land_rate / 100, 2) \
                               + self.data.sell_food * round((1 - EXCH_COMM) * self.data.food_rate / 100, 2) \
                               + self.data.sell_land * round((1 - EXCH_COMM) * self.data.land_rate / 100, 2)

        self.money_after_exchange = int(self.money_after_exchange)

        # заглушка
        if self.money_after_exchange < 0:
            logger.error(self.money_after_exchange)
            self.money_after_exchange = 0

        txt += "После операций на бирже:\n"
        txt += ":ear_of_corn: зерно " + sstr(self.food_before_seed)
        if self.food_before_seed >= self.food_before:
            txt += " (+" + sstr(self.food_before_seed - self.food_before) + ")\n"
        else:
            txt += " (" + sstr(self.food_before_seed - self.food_before) + ")\n"

        txt += ":camping: земля " + sstr(self.land_for_seed)
        if self.land_before <= self.land_for_seed:
            txt += " (+" + sstr(self.land_for_seed - self.land_before) + ")\n"
        else:
            txt += " (" + sstr(self.land_for_seed - self.land_before) + ")\n"

        txt += ":money_bag: золото " + sstr(self.money_after_exchange)
        if self.money_before <= self.money_after_exchange:
            txt += " (+" + sstr(self.money_after_exchange - self.money_before) + ")\n"
        else:
            txt += " (" + sstr(self.money_after_exchange - self.money_before) + ")\n"

        await self.call.message.answer(emoji.emojize(txt))
        await dp.bot.send_chat_action(self.call.message.chat.id, ChatActions.TYPING)
        # await asyncio.sleep(1)
        return txt

    def process_food(self):
        txt = ""
        # зерно
        # 1. Наемники
        if self.food_before_seed < (self.data.army + self.data.decret2army) * 100:
            # наемники голодные разбегаются
            self.army_after = int(self.food_before_seed / 100)
            self.food_before_seed = 1
            txt += ":warning:Наемники разбегаются от голода. Осталось " + sstr(self.army_after) + "\n"
        else:
            self.army_after = self.data.army + self.data.decret2army
            self.food_before_seed = self.food_before_seed - self.army_after * 100

        txt += ":backhand_index_pointing_right:На снабжение армии ушло :ear_of_corn:" + sstr(self.army_after * 100) + "\n"

        # 2. Посевная
        # возможно засеять
        self.land_for_seed_with_hands = min(self.land_for_seed, self.data.people * 10, max(1, self.data.decret2seed))
        if self.data.people * 10 < self.land_for_seed:
            txt += ":warning:Нехватка рук для посевной.\nЗасеять можно только :camping:" + sstr(self.data.people * 10) + "\n"
        if self.land_for_seed < self.data.decret2seed:
            txt += ":warning:Нехватка площадей под посевы.\nЗасеять можно только :camping:" + sstr(self.land_for_seed) + "\n"

        txt += ":backhand_index_pointing_right:Всего засеяли :camping:" + sstr(self.land_for_seed_with_hands) + "\n"

        if self.food_before_seed < self.land_for_seed_with_hands:
            self.decret2seed = self.food_before_seed
            self.food_after_seed = 0
            txt += ":warning:Нехватка зерна для посевной.\nЗасеяли только :camping:" + sstr(self.decret2seed) + "\n"
        else:
            self.decret2seed = self.land_for_seed_with_hands
            self.food_after_seed = self.food_before_seed - self.decret2seed

        # 3. Еда
        # food_after_seed - нераспределенный остаток
        # self.data.decret2food - запланировано на еду
        # self.data.people * 40 - надо для еды

        if self.food_after_seed < self.data.people * 40:  # self.data.decret2food:
            self.decret2food = self.food_after_seed
            self.food_after = 0
            txt += ":warning:Население голодает. На хлеб осталось :ear_of_corn:" + sstr(self.decret2food) + "\n"
        else:
            if self.food_after_seed < self.data.decret2food:
                self.decret2food = self.food_after_seed
                txt += ":warning:На хлеб осталось меньше плана :ear_of_corn:" + sstr(self.decret2food) + "\n"
                self.food_after = 0
            else:
                self.decret2food = self.data.decret2food
                self.food_after = self.food_after_seed - self.data.decret2food

            txt += ":backhand_index_pointing_right:распределение зерна: Ушло на хлеб: :ear_of_corn:" + sstr(self.decret2food) + "\n"


            
        return txt
    
    def process_people(self):

        txt = ""

        # население
        if self.decret2food < self.data.people * 40:
            # люди дохнут
            cand2dead = self.data.people - self.decret2food / 40
            self.dead = int(cand2dead * random.random())

            self.born = int(self.data.people * .02 * random.random())
            txt += ":baby:Родилось " + sstr(self.born) + "\n"

            # self.people_after = int(self.decret2food / 40)

            if self.dead > 0:
                txt += ":candle:Люди мрут от голода. Погибло " + sstr(self.dead) + "\n"
            # txt += ":busts_in_silhouette:Мигранты не приехали\n"
            self.income = 0

        else:
            self.born = int(self.data.people * .1 * random.random())
            txt += ":baby:Родилось " + sstr(self.born) + "\n"

            self.dead = int(self.data.people * .05 * random.random())
            self.income = int((self.decret2food - self.data.people * 40) / 100 * random.random()) + 1

            txt += ":candle:Умерло от старости " + sstr(self.dead) + "\n"

        if self.data.wait_migrate > 0:
            txt += ":busts_in_silhouette:На слухах о лучшей жизни приехали мигранты " + sstr(self.data.wait_migrate) + "\n"

        self.people_after = int(self.data.people - self.dead + self.born + self.data.wait_migrate)  # в базу

        return txt

    async def process_harvest(self):
        txt = ""
        # урожай

        harvest_min = 3.5
        harvest_max = 12.5

        self.harvest_rate = round(harvest_min + (harvest_max - harvest_min) * random.random(), 1)
        self.harvest = int(self.decret2seed * self.harvest_rate)

        # 4. Крысы
        if self.food_after > 0:
            txt += "Зерна в избытке. В амбарах осталось еще :ear_of_corn:" + sstr(self.food_after) + "\n"
            self.mouse_eat = int(self.harvest_rate / harvest_max / 2 * random.random() * self.food_after)
            txt += ":rat:Крысы съели :ear_of_corn:" + sstr(self.mouse_eat) + "\n"
            self.food_after = self.food_after - self.mouse_eat


        dr = self.land_rate_after / 1000
        logger.debug(dr)


        if self.harvest_rate < harvest_min + 2:

            logger.debug(1 + .25 * dr)
            self.up_rate(1+.25*dr)

            await self.call.message.answer(
                emoji.emojize(":red_circle::red_exclamation_mark:Цены на бирже следуют за слухами о плохом урожае"))

        if self.harvest_rate > harvest_max - 2:
            logger.debug(1/(1 + .25 * dr))
            self.up_rate(1/(1 + .25 * dr))

            await self.call.message.answer(
                emoji.emojize(":green_circle:Цены на бирже следуют за слухами о большом урожае"))

        txt += ":ear_of_corn:Урожай составил " + sstr(self.harvest) + " (" + str(self.harvest_rate) + " ц/га )\n"



        return txt

    async def process_caravan(self):
        txt = ""
        self.caravan_money = 0
        self.caravan_inc = 1
        # logger.debug(self.data.decret2caravan)
        if self.data.decret2caravan > 0:
            p_caravan = self.data.p_caravan * random.random()
            # logger.debug(p_caravan)
            if p_caravan > 70:
                if random.random() < .75:
                    self.caravan_money = int(10 * self.data.decret2caravan * random.random())

                    await self.call.message.answer(emoji.emojize(":chequered_flag:Вернулся караван! Доход составил " + sstr(self.caravan_money) + "!\n"))
                else:
                    self.caravan_money = 0
                    await self.call.message.answer(emoji.emojize(
                        ":skull_and_crossbones:Караван разграблен пиратами!"))

                self.caravan_inc = 0

        return txt

    async def process_war(self):
        txt = ""
        # война
        self.w_lose_food = 0
        self.w_lose_land = 0
        self.w_lose_people = 0
        self.w_lose_money = 0
        self.w_lose_army = 0

        self.w_harvest = self.data.wait_w_harvest
        self.wait_w_harvest = (differ(6,self.harvest_rate)-1)*.2+1
        self.w_land = differ(self.land_for_seed, self.land_for_seed_with_hands)
        self.w_food = (differ(self.data.people * 40, self.decret2food)-1)*.2+1
        self.w_army = differ(self.data.people, self.army_after * 10)

        self.w_money = max(1,(differ(self.money_after_exchange, (self.data.land_rate / 100 * self.land_for_seed + (self.data.food_rate / 100) * self.data.people * 50 + self.data.food_rate /100 * self.data.food ))))

        self.war_indicator = self.w_harvest * self.w_land * self.w_food * self.w_army * self.w_money

        self.p_war = self.war_indicator * self.data.p_war / 100

        # txt += str(self.p_war)

        await self.notify_user_about_status()

        logger.debug((self.wait_w_harvest, 'wait_w_harvest'))
        logger.debug((self.w_harvest, 'w_harvest'))
        logger.debug((self.w_land, 'w_land'))
        logger.debug((self.w_food, 'w_food'))
        logger.debug((self.w_army, 'w_army'))
        logger.debug((differ(self.money_after_exchange, (self.data.land_rate / 100 * self.land_for_seed + (self.data.food_rate / 100) * self.data.people * 50 + self.data.food_rate /100 * self.data.food )), 'w_money'))
        logger.debug((self.w_money, 'w_money'))
        logger.debug((self.data.p_war / 100, ':::p_war'))
        logger.debug((self.war_indicator, 'war_indicator'))
        logger.debug((self.p_war, ':::p_war - итог учетом накопительного ожидания войны'))

        self.p_war = self.p_war * random.random()
        logger.debug((self.p_war, ':::self.p_war > .5???'))





        return ""

    async def war_result(self):

        self.p_war = 0
        war = self.war_indicator * (.4 + .2 * random.random())

        logger.debug(war)

        txt2 = ""
        if war > .5:
            self.p_lose = - self.p_lose
            txt2 += "+++Наши войска потерпели поражение!+++\n"
            txt2 += "Потеряно:\n"
        else:
            txt2 += "Наши войска одержали победу!\n"
            txt2 += "Захвачено:\n"

        # Eforia.people: max(self.people_after + self.w_lose_people, 1),
        # Eforia.food: max(self.food_after + self.harvest + self.w_lose_food, 1),
        # Eforia.land: max(self.land_for_seed + self.w_lose_land, 1),
        # Eforia.money: max(self.money_after_exchange + self.caravan_money + self.w_lose_money, 1),
        # Eforia.army: max(self.army_after + self.w_lose_army, 0),

        self.w_lose_food = int(self.p_lose * max(self.food_after + self.harvest, 1) * random.random())
        self.w_lose_land = int(self.p_lose * max(self.land_for_seed, 1) * random.random())
        self.w_lose_people = int(self.p_lose * max(self.people_after, 1) * random.random())
        self.w_lose_money = int(self.p_lose * max(self.money_after_exchange, 1) * random.random())
        self.w_lose_army = int(self.p_lose * max(self.army_after, 0) * random.random())

        self.up_rate(1.25)

        txt2 += ":family: " + sstr(self.w_lose_people) + " \n"
        txt2 += ":ear_of_corn: " + sstr(self.w_lose_food) + "\n"
        txt2 += ":camping: " + sstr(self.w_lose_land) + "\n"
        txt2 += ":money_bag: " + sstr(self.w_lose_money) + "\n"
        txt2 += ":guard: " + sstr(self.w_lose_army) + "\n"
        txt2 += "------------------------------\n"

        return txt2

        # await self.call.message.answer(emoji.emojize(txt2))

    async def notify_user_about_status(self):



        # урожай
        if self.w_harvest > 1.11: await self.call.message.answer(
            emoji.emojize(":red_circle::red_exclamation_mark:Низкий урожай прошлого года породил продовольственный кризис! Враг у ворот!..."))
        elif self.w_harvest > 1.07: await self.call.message.answer(
            emoji.emojize(":orange_circle::red_exclamation_mark:Голод неурожая прошлого года может спровоцировать войну..."))
        elif self.w_harvest > 1.03: await self.call.message.answer(
            emoji.emojize(":yellow_circle::warning:Слухи о плохом урожае прошлого года достигли заграницы..."))
        elif self.w_harvest > 1: await self.call.message.answer(
            emoji.emojize(":warning:Население не довольно низким урожаем прошлого года..."))
        elif self.w_harvest > .85: await self.call.message.answer(
            emoji.emojize(":backhand_index_pointing_right:Урожай прошлого года позволяет быть населению в достатке..."))
        else:
            await self.call.message.answer(
                emoji.emojize(":folded_hands:Население с прошлого года урожайного изобилия славит богов и мудрость правителя..."))

        await dp.bot.send_chat_action(self.call.message.chat.id, ChatActions.TYPING)
        # await asyncio.sleep(1)

        # лишние земли
        if self.w_land > 1.4:
            await self.call.message.answer(emoji.emojize(":red_circle::red_exclamation_mark:Катастрофа из-за бесхоза неминуема..."))
        elif self.w_land > 1.15:
            await self.call.message.answer(emoji.emojize(":orange_circle::red_exclamation_mark:Очень опасно иметь большой избыток земли..."))
        elif self.w_land > 1.08:
            await self.call.message.answer(emoji.emojize(":yellow_circle::warning:Соседи готовы захватить бесхозные земли..."))
        elif self.w_land > 1.03:
            await self.call.message.answer(emoji.emojize(":green_circle:Запустение в некоторых землях привлекает врагов..."))
        elif self.w_land > 1.0:
            await self.call.message.answer(emoji.emojize(":warning:Необходимо позаботиться о пустующих землях..."))
        elif self.w_land < 1.0:
            await self.call.message.answer(
                emoji.emojize(":backhand_index_pointing_right:Население может обработать больше земель. Растет число дармоедов..."))

        await dp.bot.send_chat_action(self.call.message.chat.id, ChatActions.TYPING)
        # await asyncio.sleep(1)

        # количество хлеба на еду
        if self.w_food > 1.1:
            await self.call.message.answer(emoji.emojize(":warning::red_exclamation_mark:Недовольство нехваткой хлеба угрожает процветанию..."))
        elif self.w_food > 1:
            await self.call.message.answer(emoji.emojize(":warning:Разумным было бы увеличить нормы выдачи хлеба..."))
        elif self.w_food < 1:
            await self.call.message.answer(emoji.emojize(":backhand_index_pointing_right:Достаток в еде способствует приросту населения..."))

        await dp.bot.send_chat_action(self.call.message.chat.id, ChatActions.TYPING)
        # await asyncio.sleep(1)


        if self.w_army > 1.4:
            await self.call.message.answer(emoji.emojize(":red_circle::red_exclamation_mark:Срочно нужны войска! Катастрофа неминуема!..."))
        elif self.w_army > 1.2:
            await self.call.message.answer(emoji.emojize(":orange_circle::red_exclamation_mark:Низкая беспособность армии! Примите меры!..."))
        elif self.w_army > 1.1:
            await self.call.message.answer(emoji.emojize(":yellow_circle:Безопасность государства может обеспечить только сильная армия..."))
        elif self.w_army > 1:
            await self.call.message.answer(emoji.emojize(":green_circle:Не забывайте! Армия - опора власти..."))
        else:
            await self.call.message.answer(emoji.emojize(":backhand_index_pointing_right:Избыток наемников приводит к лишней трате хлеба"))

        await dp.bot.send_chat_action(self.call.message.chat.id, ChatActions.TYPING)
        # await asyncio.sleep(1)


        if self.w_money > 1.2:
            await self.call.message.answer(emoji.emojize(":red_circle::red_exclamation_mark:Критический уровень расслоения в обществе грозит катастрофой!..."))
        elif self.w_money > 1.1:
            await self.call.message.answer(emoji.emojize(":orange_circle::red_exclamation_mark:Расслоение в обществе достигло критического уровня..."))
        elif self.w_money > 1:
            await self.call.message.answer(emoji.emojize(":yellow_circle:Возросло расслоение в обществе..."))

        await dp.bot.send_chat_action(self.call.message.chat.id, ChatActions.TYPING)
        # await asyncio.sleep(1)


        if self.p_war > .9:
            await self.call.message.answer(emoji.emojize(":red_circle::double_exclamation_mark:Напряженность на границах взрывоопасна!"))
        elif self.p_war > .8:
            await self.call.message.answer(emoji.emojize(":orange_circle::red_exclamation_mark:Напряженность на границах требует решительных мер!"))
        elif self.p_war > .7:
            await self.call.message.answer(emoji.emojize(":yellow_circle::red_exclamation_mark:Напряженность на границах вызывает озабоченность..."))
        elif self.p_war > .5:
            await self.call.message.answer(emoji.emojize(":warning:Сообщают о признаках напряженности на границах..."))
        else:
            await self.call.message.answer(emoji.emojize(":green_circle:На границе всё спокойно..."))

        await dp.bot.send_chat_action(self.call.message.chat.id, ChatActions.TYPING)
        # await asyncio.sleep(1)

        logger.debug(self.war_indicator)
        if self.war_indicator > 1.0:
            await self.call.message.answer(emoji.emojize(
                ":red_circle::double_exclamation_mark:Угрожающе низкий уровень готовности империи к войне!\n"))
        elif self.war_indicator > .95:
            await self.call.message.answer(emoji.emojize(
                ":orange_circle::red_exclamation_mark:Слабый уровень готовности империи к войне!\n"))
        elif self.war_indicator > .9:
            await self.call.message.answer(
                emoji.emojize(":yellow_circle:Надо увеличить уровень готовности империи к войне!\n"))
        elif self.war_indicator > .85:
            await self.call.message.answer(emoji.emojize(":green_circle:Хороший уровень готовности империи к войне!\n"))
        else:
            await self.call.message.answer(emoji.emojize(
                ":backhand_index_pointing_right:Идеальный уровень готовности империи к войне!\n"))

        await dp.bot.send_chat_action(self.call.message.chat.id, ChatActions.TYPING)
        # await asyncio.sleep(1)

        # караван
        if self.data.decret2caravan > 0:
            if self.data.p_caravan >= 90:
                await self.call.message.answer(
                    emoji.emojize(":camel::two-hump_camel::camel:На границе замечены клубы пыли. Караван на подходе"))
            elif self.data.p_caravan >= 70:
                await self.call.message.answer(
                    emoji.emojize(":camel::two-hump_camel::camel:Гонец сообщает о скором прибытии каравана"))
            elif self.data.p_caravan >= 50:
                await self.call.message.answer(
                    emoji.emojize(
                        ":camel::two-hump_camel::camel:Торговцы закончили продажу своего товара и собираются восвояси"))
            elif self.data.p_caravan >= 30:
                await self.call.message.answer(
                    emoji.emojize(":camel::two-hump_camel::camel:Караван достиг одного из заморских рынков"))
            elif self.data.p_caravan >= 10:
                await self.call.message.answer(
                    emoji.emojize(":camel::two-hump_camel::camel:Караван покинул пределы империи по пути к заморским странам"))

    async def random_tip(self):
        headers = [
            ':index_pointing_up:Вот оно как:\n:index_pointing_up:',
            ':index_pointing_up:Прикинь:\n:index_pointing_up:',
            ':index_pointing_up:Кто бы мог подумать:\n:index_pointing_up:',
            ':index_pointing_up:Примечательно:\n:index_pointing_up:',
            ':index_pointing_up:На заметку:\n:index_pointing_up:',
            ':index_pointing_up:Мужики такой печали отродяся не видали:\n:index_pointing_up:',
            ':index_pointing_up:Как ни странно:\n:index_pointing_up:',
            ':index_pointing_up:Обычно:\n:index_pointing_up:',
            ':index_pointing_up:Если по инструкции, то:\n:index_pointing_up:',
        ]
        tips = [
            'Нехватка зерна восполняется за счет еды. Потом пострадают посевы. Армия - последняя в этом списке.',
            'Усиленное питание населения после неурожайного года - лучший способ победить, когда война неизбежна',
            'Гармония - это когда:\n10:family: : 400:ear_of_corn: : 100:camping: : 1:guard:',
            'Один крестьянин не может засеять более 10 га земли',
            'Чтобы не голодать одному жителю достаточно :ear_of_corn:40 на год',
            'Выделяешь меньше :ear_of_corn:40 на еду в год - способствушь росту смертности и снижению рождаемости населения:family:',
            'Наемник в любом случае съест :ear_of_corn:100 за год',
            'Содержание более одного :guard:наемника на каждые 10:family: - лишняя трата ресурсов',
            'Чем больше проблем в государстве, тем выше риск начала войны.',
            'Когда имется много критических проблем в стране увеличивает риск проиграть войну',
            'Войны невозможно исключить. Возможно только отсрочить. Порядок в стране - успех на поле боя',
            'Гармоничное развитие - залог процветания империи',
            'Ваши закупки на бирже тянут цены вниз.',
            'Перекормил население - жди на следующий год приток мигрантов',
            'Не забудь засеять поля и накормить народ!',
            'Новый караван можно снарядить только после возвращения предыдущего.',
            'Одного :guard:наемника на 10:family: крестьян обычно вполне достаточно',
            'Изобилие в империи хлеба способствует росту :family:населения и притоку :busts_in_silhouette:мигрантов',
            'Нераспределенные запасы - страховка на случай неурожая',
            'Нераспределенные запасы :ear_of_corn: будут атаковать :rat: грызуны',
            'Даже если кажется, что война неминуема, хороший урожай может дать передышку. Используй ее с умом!',
            'Уровень готовности империи к войне зависит от многих факторов!',
            'Низкий урожай может спровоцировать войну!',
            'Из-за большого числа необрабатываемых земели соседи могут напасть на империю!',
            'Голод в империи подталкивает жадных соседей на агрессию!',
            'Слабая армия не предотвратит войны!',
        ]
        await self.call.message.answer(emoji.emojize(random.choice(headers) + random.choice(tips)))

    async def random_news(self):
        itemses = [
            ':cow_face:говядина',
            ':deer:оленина',
            ':pig:свинина',
            ':ram:баранина',
            ':chicken:птица',
            ':frog:лягушачьи лапки',
            ':fish:рыба',
            ':octopus:морепродукты',
            ':snail:экзотические продукты',
            ':lemon:фрукты',
            ':strawberry:ягоды',
            ':carrot:овощи',
            ':waffle:вафли',
            ':cheese_wedge:сыр',
            ':egg:яйца',
            ':butter:масло',
            ':lobster:раки',
            ':beer_mug:пиво',
            ':brick:кирпичи',
            ':kick_scooter:самокаты',
            ':umbrella:зонтики',
            ':balloon:шарики',
            ':teddy_bear:игрушки',
            ':briefs:труселя',
            ':bikini:купальники',
            ':socks:носки',
            ':glasses:очки',
            ':gem_stone:драгоценности',
            ':pencil:карандаши',
            ':briefcase:портфели',
            ':paperclip:скрепки',
            ':key:ключи',

        ]
        locates = [
            'На :snowflake:северных рынках империи ',
            'На :umbrella_on_ground:южных рынках империи ',
            'На :stadium:западных рынках империи ',
            'На :mahjong_red_dragon:восточных рынках империи ',
        ]
        notes = [
            'исчезли из продажи ',
            'появились в изобилии ',
                 ]

        itm = random.choice(itemses)
        itemses.remove(itm)
        txt = random.choice(locates) + random.choice(notes) + itm + " и " + random.choice(itemses)
        await self.call.message.answer(emoji.emojize(txt))
        
    def garmony(self):

        g_people = differ(self.data.people, 100*1.05**self.data.old)
        g_food_people = differ(self.data.food, (self.data.people * 50 + 100 * self.data.army) * 1.5)
        g_army_people = differ(10 * self.data.army, self.data.people)
        g_land_people = differ(self.data.land, self.data.people * 10)
        g_money = differ(self.data.money, (self.data.land_rate / 100 * self.data.land + (
                    self.data.food_rate / 100) * self.data.people * 50 + self.data.food_rate / 100 * self.data.food))

        # logger.debug((g_people, 'g_people'))
        # logger.debug((g_food_people, 'g_food_people'))
        # logger.debug((g_army_people, 'g_army_people'))
        # logger.debug((g_land_people, 'g_land_people'))
        # logger.debug((g_money, 'g_money'))

        # g_people = (1 - g_people) ** 2
        # g_food_people = (1 - g_food_people) ** 2
        # g_army_people = (1 - g_army_people) ** 2
        # g_land_people = (1 - g_land_people) ** 2

        ss = g_people * g_food_people * g_army_people * g_land_people * g_money
        logger.debug((ss, 'ss'))
        diff = differ(ss,2**5)
        logger.debug((diff, 'diff'))
        garmony = round(diff * self.data.old, 2)
        return garmony

    def up_rate(self, k):
        self.food_rate_after = min(50000, max(2, self.food_rate_after * k))
        self.land_rate_after = min(50000, max(2, self.land_rate_after / k))

    def upsert(self):
        # Обновление империи
        # logger.debug(20*(self.w_harvest + self.w_land + self.w_food + self.w_army + self.w_money))
        # logger.debug((self.data.food_vol, self.data.buy_food , self.data.sell_food, self.data.land_vol, self.data.buy_land , self.data.sell_land))
        # logger.debug(self.data.log_garmony)
        self.data.log_garmony.append(max(self.data.garmony, self.garmony()))
        self.data.log_people.append(str(max(self.people_after + self.w_lose_people, 1)))
        self.data.log_land.append(str(max(self.land_for_seed + self.w_lose_land, 1)))
        self.data.log_army.append(str(max(self.army_after + self.w_lose_army, 0)))
        self.data.log_money.append(str(max(self.money_after_exchange + self.caravan_money + self.w_lose_money, 1)))
        self.data.log_event.append(str(self.harvest_rate))
        logger.debug(self.data.log_garmony)

        updated = {
            Eforia.old: self.data.old + 1,
            Eforia.people: max(self.people_after + self.w_lose_people, 1),
            Eforia.food: max(self.food_after + self.harvest + self.w_lose_food, 1),
            Eforia.land: max(self.land_for_seed + self.w_lose_land, 1),
            Eforia.money: max(self.money_after_exchange + self.caravan_money + self.w_lose_money, 1),
            Eforia.army: max(self.army_after + self.w_lose_army, 0),
            Eforia.p_war: int(self.data.p_war + 15 * self.war_indicator),
            # Eforia.p_hunger
            Eforia.p_caravan: (self.data.p_caravan + 20) * self.caravan_inc,
            Eforia.decret2food: 0,
            Eforia.decret2seed: 0,
            Eforia.decret2caravan: self.data.decret2caravan * self.caravan_inc,
            Eforia.decret2army: 0,
            Eforia.buy_food: 0,
            Eforia.buy_land: 0,
            Eforia.sell_food: 0,
            Eforia.sell_land: 0,
            Eforia.food_rate: self.food_rate_after,
            Eforia.land_rate: self.land_rate_after,
            Eforia.food_vol: max((self.data.food_vol + self.data.buy_food + self.data.sell_food) *.9,5000),
            Eforia.land_vol: max((self.data.land_vol + self.data.buy_land + self.data.sell_land)*.9,1000),
            Eforia.wait_migrate: int(self.income),
            Eforia.wait_w_harvest: self.wait_w_harvest,
            Eforia.garmony: max(self.data.garmony, self.garmony()),

            Eforia.log_garmony: "|".join(list(map(str, self.data.log_garmony))),
            Eforia.log_people: "|".join(list(map(str, self.data.log_people))),
            Eforia.log_land: "|".join(list(map(str, self.data.log_land))),
            Eforia.log_army: "|".join(list(map(str, self.data.log_army))),
            Eforia.log_money: "|".join(list(map(str, self.data.log_money))),
            Eforia.log_event: "|".join(list(map(str, self.data.log_event))),


        }
        Eforia.update(updated).where(Eforia.telega_id == self.chat_id).execute()
        self.data = Eforia.get(Eforia.telega_id == self.chat_id)