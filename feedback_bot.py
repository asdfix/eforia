import emoji
import re
from aiogram import executor
from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.utils.markdown import hlink

from bot.utils.misc.logging import logger
from db.models import Eforia

bot = Bot(token='5140311734:AAFw5wDsAm8u8rHvb5MBlNI6XseahJSXx_4', parse_mode=types.ParseMode.HTML)
ADMINS=541830579,#,  1774746502 183767040

storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
from bot.utils.notify_admins import on_startup_notify
from aiogram.dispatcher.filters.builtin import CommandStart


async def on_startup(dispatcher):
    # Уведомляет про запуск
    await on_startup_notify(dispatcher)

@dp.message_handler(CommandStart(), state="*")
async def bot_start(message: types.Message):  # , state: FSMContext

    # logger.info('bot_start')

    txt = f'''
    :waving_hand:Привет!:waving_hand:
Я почтовый бот. Мне повезло, меня создал мой хозяин, хотя у него обычно очень мало времени.\n
Могу отправить голубиной почтой твое текстовое сообщение создателям проекта @EforiaGodBot.

Медиа-контент я передавать не умею. По опыту пересылки также могу сказать, что бессмысленные сообщения моим создателем игнорируются.\n
Можешь приступать:backhand_index_pointing_down:
'''

    if int(message.chat.id) in ADMINS:  # обрабатываем месс админа
        certies = Eforia.select(Eforia.telega_id).distinct()

        txt += "\n\nПользователей: {}".format(len(certies))

    return await message.answer(emoji.emojize(txt))


@dp.message_handler()
async def echo_message(msg: types.Message):    
    
    if int(msg.chat.id) in ADMINS: # обрабатываем месс админа
    
        try:
    
            exemple = re.search(r'##(.*?)##(.*?)##\n(.*?)\n(.*?)$', msg.reply_to_message.text)
            chat = exemple.group(1)
            ms_id = exemple.group(2)
            # ms_txt = exemple.group(3)
            
            await msg.answer(text = "OK")
            
            # ответ пользователю
            return await dp.bot.send_message(chat_id=chat, reply_to_message_id=ms_id, text=msg.text)
            # await dp.bot.forward_message(msg.reply_to_message.forward_from.id, from_chat_id=msg.reply_to_message.forward_from_chat.id, message_id=msg.message_id)
        except:
            return await msg.answer(text = "Нужно ответить с реплаем пользователя!")

    else:  # обрабатываем месс пользователя

        utxt = f'''Принято! Спасибо!\nКак админ ответит - сразу дам знать.\n\nВажно! Я не смогу переслать ответ админа, если этот чат будет удален!\n\n'''
        utxt += hlink('Поддержи наш проект!', 'https://pay.cloudtips.ru/p/d0ef58d2')
        await msg.answer(text=utxt, disable_web_page_preview=True)

        # utxt += '<a href="https://pay.cloudtips.ru/p/d0ef58d2">Поддержи наш проект!</a>'
        # await msg.answer(text=utxt, parse_mode="HTML")

        # utxt = hlink('Поддержи наш проект!', 'https://pay.cloudtips.ru/p/d0ef58d2')
        # return call.message.answer(text=utxt)
        # return call.message.answer(text='<a href="https://pay.cloudtips.ru/p/d0ef58d2">Поддержи наш проект!</a>',parse_mode="HTML")

    
        txt = "##{}##{}##\n{} {}\n{}".format(msg.chat.id, msg.message_id, msg.chat.first_name, msg.chat.last_name, msg.text)
        for admin in ADMINS:
            try:
                await dp.bot.send_message(chat_id=admin, text=txt)
                # await dp.bot.forward_message(admin, from_chat_id=msg.chat.id, message_id=msg.message_id) #, disable_notification=False
            except Exception as err:
                pass

if __name__ == '__main__':    

    executor.start_polling(dp, on_startup=on_startup)