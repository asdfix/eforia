# import logging

# from config import *

from aiogram import executor

from loader import dp
from bot.utils.notify_admins import on_startup_notify


async def on_startup(dispatcher):
    # Уведомляет про запуск
    await on_startup_notify(dispatcher)


if __name__ == '__main__':

    # logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
    #                     level=logging.DEBUG)

    executor.start_polling(dp, on_startup=on_startup)
